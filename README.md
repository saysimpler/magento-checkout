# Simpler Checkout Magento 2 Module

This is the repository holding the code for the [Simpler Checkout](https://simpler.so) Magento 2 Module.

You can find extensive documentation on installing & configuring Simpler for Magento 2 [here](https://docs.simpler.so/category/adobe-commerce-magento-2)

## Version Compatibility

The minimum Magento version currently supported is v2.4.0.

## Installation

```shell
composer require simpler/module-checkout"
bin/magento module:enable Simpler_Checkout
bin/magento setup:upgrade
bin/magento setup:di:compile
bin/magento setup:static-content:deploy
```
