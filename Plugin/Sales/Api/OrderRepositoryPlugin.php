<?php

namespace Simpler\Checkout\Plugin\Sales\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface;
use Simpler\Checkout\Api\OrderExtensionAttributesRepositoryInterface;
use Simpler\Checkout\Model\OrderExtensionAttributesRepository;

class OrderRepositoryPlugin
{
    /**
     * @var OrderExtensionAttributesRepository
     */
    private $orderExtensionAttributesRepository;

    public function __construct(OrderExtensionAttributesRepositoryInterface $orderExtensionAttributesRepository)
    {
        $this->orderExtensionAttributesRepository = $orderExtensionAttributesRepository;
    }

    /**
     * Upsert to simpler_checkout_order_extension_attributes table when an order is saved.
     *
     * @param  OrderRepositoryInterface  $subject
     * @param  OrderInterface  $result
     * @return OrderInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function afterSave(OrderRepositoryInterface $subject, OrderInterface $result)
    {
        $extensionAttributes = $result->getExtensionAttributes();
        if (!$extensionAttributes || ($extensionAttributes = $extensionAttributes->getSimplerCheckout()) === null) {
            return $result;
        }

        // Set order_id foreign key
        $extensionAttributes->setOrderId($result->getIncrementId());
        $this->orderExtensionAttributesRepository->save($extensionAttributes);

        return $result;
    }

    /**
     * Attach OrderExtensionAttributes to an order when the latter is fetched from the DB.
     *
     * @param  OrderRepositoryInterface  $subject
     * @param  OrderInterface  $result
     * @param $orderId
     * @return OrderInterface
     */
    public function afterGet(OrderRepositoryInterface $subject, OrderInterface $result, $orderId)
    {
        try {
            $extensionAttributes = $this->orderExtensionAttributesRepository->getBy(OrderExtensionAttributesInterface::ORDER_ID, $result->getIncrementId());
        } catch (NoSuchEntityException $e) {
            return $result;
        }

        $result->getExtensionAttributes()->setSimplerCheckout($extensionAttributes);
        return $result;
    }
}
