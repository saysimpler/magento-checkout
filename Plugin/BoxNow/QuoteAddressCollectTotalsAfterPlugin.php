<?php

namespace Simpler\Checkout\Plugin\BoxNow;

use Magento\Framework\Event\Observer;
use Magento\Quote\Model\Quote;
use Simpler\Checkout\Gateway\Config\ConfigProvider;

/**
 * It wraps the Elegento Box Now plugin and sets shipping description of collected address total.
 *  Without this the shipping description of an order will be Box Now - Box Now instead of the locker data.
 * Check di.xml for the hardwiring.
 *
 * @see \Elegento\BoxNow\Observer\Frontend\Sales\QuoteAddressCollectTotalsAfter::execute()
 */
class QuoteAddressCollectTotalsAfterPlugin
{
    public function afterExecute($quoteAddressCollectTotalsAfter, $noIdeaWhatThisMightBe, Observer $observer) {
        $quote = $observer->getEvent()->getQuote();
        $addressTotal = $observer->getEvent()->getTotal();
        if ($quote instanceof Quote
            && $quote->getShippingAddress()
            && $quote->getShippingAddress()->getShippingMethod() == 'boxnow_boxnow'
            && $quote->getPayment()
            && $quote->getPayment()->getData('method') == ConfigProvider::CODE
            && $addressTotal
        ) {
            $addressTotal->setShippingDescription($quote->getShippingAddress()->getShippingDescription());
        }
    }
}

