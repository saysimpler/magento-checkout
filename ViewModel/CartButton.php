<?php

namespace Simpler\Checkout\ViewModel;

use Magento\Checkout\Block\Cart as Block;

class CartButton extends AbstractButton
{

    /**
     * @param Block $block
     */
    public function getCartPayload($block)
    {
        return [
            'coupon' => $block->getQuote()->getCouponCode(),
            'items' => $this->buttonHelper->buildCart($block->getQuote()->getAllVisibleItems())
        ];
    }
}
