<?php

namespace Simpler\Checkout\ViewModel;

use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Simpler\Checkout\Helper\ButtonHelper;

abstract class AbstractButton implements ArgumentInterface
{

    /**
     * @var ButtonHelper
     */
    protected $buttonHelper;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    protected $productAttributeRepository;

    public function __construct(
        ButtonHelper $buttonHelper,
        ProductRepositoryInterface $productRepository,
        ProductAttributeRepositoryInterface $productAttributeRepository
    ) {
        $this->buttonHelper = $buttonHelper;
        $this->productRepository = $productRepository;
        $this->productAttributeRepository = $productAttributeRepository;
    }

    /**
     * @param Magento\Framework\View\Element\AbstractBlock;
     */
    abstract public function getCartPayload($block);

    public function shouldRender(): bool
    {
        return $this->buttonHelper->shouldRender();
    }

    /**
     * @param Magento\Framework\View\Element\AbstractBlock;
     */
    public function getPayload($block)
    {
        return $this->buttonHelper->getPayload($this->getCartPayload($block));
    }

    /**
     * Get App ID
     *
     * @return string|null
     */
    public function getAppId(): ?string
    {
        return $this->buttonHelper->getAppId();
    }

    /**
     * Get Current Locale
     *
     * @return string
     */
    public function getLocale(): string
    {
        return $this->buttonHelper->getLocale();
    }
}
