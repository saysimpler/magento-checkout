<?php

namespace Simpler\Checkout\ViewModel;

use Magento\Catalog\Block\Product\View as Block;

class SimpleProductButton extends AbstractButton
{
    /**
     * @param Block $block
     */
    public function getCartPayload($block)
    {
        return [
            'items' => [$this->buttonHelper->buildCartItem($block->getProduct())]
        ];
    }
}
