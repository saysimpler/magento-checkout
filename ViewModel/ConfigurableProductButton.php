<?php

namespace Simpler\Checkout\ViewModel;

use Magento\Catalog\Block\Product\View as Block;

class ConfigurableProductButton extends AbstractButton
{

    /**
     * @param Block $block;
     */
    public function getCartPayload($block)
    {
        $product = $block->getProduct();
        $configurableOpts = $product->getTypeInstance()->getConfigurableOptions($product);
        $configurations = [];
        foreach ($configurableOpts as $opts) {
            foreach ($opts as $attr) {
                $a = $this->productAttributeRepository->get($attr['attribute_code']);
                if (method_exists($a, 'getId')) {
                    $configurations[$attr['sku']]['configuration'][$a->getId()] = $attr['value_index'];
                    $configurations[$attr['sku']]['product'] = $this->buttonHelper->buildCartItem(
                        $this->productRepository->get($attr['sku'])
                    );
                }
            }
        }

        return [
            'items' => [[
                'product_sku' => $product->getSku(),
                'product_type' => $product->getTypeId(),
                'configurations' => $configurations,
            ]]
        ];
    }
}
