<?php

namespace Simpler\Checkout\Api\Json;

/**
 * Interface which represents associative array item with string as value.
 */

 /**
 * @api
 */
interface AssociativeArrayItemInterface
{
    const KEY = "key";
    const VALUE = "value";

    /**
     * Set key
     * @param string $key
     * @return \Simpler\Checkout\Api\Json\AssociativeArrayItemInterface
     */
    public function setKey(string $key);

    /**
     * Get key
     * @return string
     */
    public function getKey();

    /**
     * Set value
     * @param string $value
     * @return \Simpler\Checkout\Api\Json\AssociativeArrayItemInterface
     */
    public function setValue(string $value);


    /**
     * Get value
     * @return string
     */
    public function getValue();
}