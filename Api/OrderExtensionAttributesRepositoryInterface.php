<?php

namespace Simpler\Checkout\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface;

interface OrderExtensionAttributesRepositoryInterface
{
    /**
     * @param  string  $column
     * @param mixed $value
     *
     * @return OrderExtensionAttributesInterface
     * @throws NoSuchEntityException
     */
    public function getBy(string $column, $value);

    /**
     * @param  \Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface  $extensionAttributes
     * @return \Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface
     */
    public function save(\Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface $extensionAttributes);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param  string  $column
     * @param mixed $value
     *
     * @return OrderExtensionAttributesInterface
     * @throws NoSuchEntityException
     */
    public function deleteBy(string $column, $value);
}
