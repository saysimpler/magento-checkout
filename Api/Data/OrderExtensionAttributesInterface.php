<?php

namespace Simpler\Checkout\Api\Data;

interface OrderExtensionAttributesInterface
{
    const ID               = 'id';
    const ORDER_ID         = 'order_id';
    const SIMPLER_ORDER_ID = 'simpler_order_id';
    const INVOICE          = 'invoice';

    /**
     * @return string
     */
    public function getOrderId();

    /**
     * @param  string  $orderId
     *
     * @return \Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface
     */
    public function setOrderId($orderId);

    /**
     * @return string
     */
    public function getSimplerOrderId();

    /**
     * @param  string  $simplerOrderId
     *
     * @return \Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface
     */
    public function setSimplerOrderId(string $simplerOrderId);

    /**
     * @return \Simpler\Checkout\Api\Data\OrderInvoiceInterface
     */
    public function getInvoice();

    /**
     * @param  \Simpler\Checkout\Api\Data\OrderInvoiceInterface  $invoice
     * @return \Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface
     */
    public function setInvoice(OrderInvoiceInterface $invoice);
}
