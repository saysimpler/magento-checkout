<?php

namespace Simpler\Checkout\Api\Data;

interface ProductsResponseInterface extends ResponseInterface
{

    const ITEMS = "items";

    /**
     * Set Items
     *
     * @param  \Simpler\Checkout\Api\Data\ItemResponseInterface[]  $items
     * @return \Simpler\Checkout\Api\Data\ProductsResponseInterface
     */
    public function setItems(array $items);

    /**
     * Get Items
     *
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface[]
     */
    public function getItems();

}
