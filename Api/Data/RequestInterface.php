<?php

namespace Simpler\Checkout\Api\Data;

interface RequestInterface
{

    const REQUEST_ID = "request_id";
    const STORE_ID = "store_id";
    const WEBSITE_ID = "website_id";

    /**
     * Set request id
     *
     * @param  string  $requestId
     * @return \Simpler\Checkout\Api\Data\RequestInterface
     */
    public function setRequestId(string $requestId): RequestInterface;

    /**
     * Get request id
     *
     * @return string
     */
    public function getRequestId();

     /**
     * Set store id
     *
     * @param  int  $storeId
     * @return \Simpler\Checkout\Api\Data\RequestInterface
     */
    public function setStoreId(int $storeId): RequestInterface;

    /**
     * Get store id
     *
     * @return int
     */
    public function getStoreId();

    /**
     * Set website id
     *
     * @param  int  $websiteId
     * @return \Simpler\Checkout\Api\Data\RequestInterface
     */
    public function setWebsiteId(int $websiteId): RequestInterface;

    /**
     * Get store id
     *
     * @return int
     */
    public function getWebsiteId();
}
