<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface FeeInterface
{
    const TITLE = "title";
    const TOTAL_CENTS = "total_cents";

    /**
     * Set total cents
     * @param int $totalCents
     * @return \Simpler\Checkout\Api\Data\FeeInterface
     */
    public function setTotalCents(int $totalCents);

    /**
     * Get total cents
     * @return int
     */
    public function getTotalCents();

    /**
     * Set fee title
     * @param string $title
     * @return \Simpler\Checkout\Api\Data\FeeInterface
     */
    public function setTitle(string $title);

    /**
     * Get fee title
     * @return string
     */
    public function getTitle();
}
