<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface ItemCustomValuesInterface
{

    const KEY = "key";
    const VALUE = "value";

    /**
     * Set Key
     * @param string $key
     * @return \Simpler\Checkout\Api\Data\ItemCustomValuesInterface
     */
    public function setKey(string $key);

    /**
     * Get Key
     * @return string
     */
    public function getKey();

    /**
     * Set Value
     * @param string $value
     * @return \Simpler\Checkout\Api\Data\ItemCustomValuesInterface
     */
    public function setValue(string $value);

    /**
     * Get Value
     * @return string
     */
    public function getValue();
}
