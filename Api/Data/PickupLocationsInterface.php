<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface PickupLocationsInterface
{
    const PICKUP_LOCATIONS = "pickup_locations";

    /**
     * @return \Simpler\Checkout\Api\Data\PickupLocationInterface[]
     */
    public function getPickupLocations();

    /**
     * @param  \Simpler\Checkout\Api\Data\PickupLocationInterface[]  $locations
     * @return \Simpler\Checkout\Api\Data\PickupLocationsInterface
     */
    public function setPickupLocations(array $locations);

    /**
     * @param  \Simpler\Checkout\Api\Data\PickupLocationInterface  $location
     * @return \Simpler\Checkout\Api\Data\PickupLocationsInterface
     */
    public function addPickupLocation(PickupLocationInterface $location);
}
