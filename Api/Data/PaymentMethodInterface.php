<?php

namespace Simpler\Checkout\Api\Data;

interface PaymentMethodInterface
{

    const ID = "id";
    const TYPE = "type";
    const NAME = "name";
    const NET_CENTS = "net_cents";
    const TAX_CENTS = "tax_cents";
    const TOTAL_CENTS  = "total_cents";

    /**
     * @param string $id
     * @return \Simpler\Checkout\Api\Data\PaymentMethodInterface
     */
    public function setId(string $id);

    /**
     * @return string
     */
    public function getId();

    /**
     * @param string $type
     * @return \Simpler\Checkout\Api\Data\PaymentMethodInterface
     */
    public function setType(string $type);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param string $name
     * @return \Simpler\Checkout\Api\Data\PaymentMethodInterface
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param int $cents
     * @return \Simpler\Checkout\Api\Data\PaymentMethodInterface
     */
    public function setNetCents(int $cents);

    /**
     * @return int
     */
    public function getNetCents();

    /**
     * @param int $cents
     * @return \Simpler\Checkout\Api\Data\PaymentMethodInterface
     */
    public function setTaxCents(int $cents);

    /**
     * @return int
     */
    public function getTaxCents();

    /**
     * @param int $cents
     * @return \Simpler\Checkout\Api\Data\PaymentMethodInterface
     */
    public function setTotalCents(int $cents);

    /**
     * @return int
     */
    public function getTotalCents();
}
