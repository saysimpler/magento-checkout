<?php

namespace Simpler\Checkout\Api\Data;

interface QuoteResponseInterface extends ResponseInterface
{

    const QUOTATIONS = "quotations";

    /**
     * @param  \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface[]  $quotations
     * @return \Simpler\Checkout\Api\Data\QuoteResponseInterface
     */
    public function setQuotations(array $quotations);

    /**
     * @return \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface[]
     */
    public function getQuotations();
}
