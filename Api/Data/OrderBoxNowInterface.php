<?php

namespace Simpler\Checkout\Api\Data;

interface OrderBoxNowInterface
{
    const LOCKER_ID = 'locker_id';
    const ADDRESS   = 'address';
    const POSTCODE  = 'postcode';

    /**
     * @return string
     */
    public function getLockerId();

    /**
     * @param  string  $lockerId
     *
     * @return \Simpler\Checkout\Api\Data\OrderBoxNowInterface
     */
    public function setLockerId(string $lockerId);

    /**
     * @return string
     */
    public function getAddress();

    /**
     * @param  string  $address
     *
     * @return \Simpler\Checkout\Api\Data\OrderBoxNowInterface
     */
    public function setAddress(string $address);

    /**
     * @return string
     */
    public function getPostcode();

    /**
     * @param  string  $postcode
     *
     * @return \Simpler\Checkout\Api\Data\OrderBoxNowInterface
     */
    public function setPostcode(string $postcode);
}
