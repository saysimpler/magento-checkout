<?php

namespace Simpler\Checkout\Api\Data;

interface ShippingOptionInterface
{

    const ID = "id";
    const NAME = "name";
    const COST_CENTS = "cost_cents";
    const TOTAL_CENTS = "total_cents";
    const TAX_CENTS = "tax_cents";
    const TYPE = "type";

    /**
     * Set Id
     * @param string $id
     * @return \Simpler\Checkout\Api\Data\ShippingOptionInterface
     */
    public function setId(string $id);

    /**
     * Get Id
     * @return string
     */
    public function getId();

    /**
     * Set name
     * @param string $name
     * @return \Simpler\Checkout\Api\Data\ShippingOptionInterface
     */
    public function setName(string $name);

    /**
     * Get Name
     * @return string
     */
    public function getName();

    /**
     * Set Cost in Cents
     * @param int $costCents
     * @return \Simpler\Checkout\Api\Data\ShippingOptionInterface
     */
    public function setCostCents(int $costCents);

    /**
     * Get Cost in Cents
     * @return int
     */
    public function getCostCents();

    /**
     * Set Tax in Cents
     * @param int $taxCents
     * @return \Simpler\Checkout\Api\Data\ShippingOptionInterface
     */
    public function setTaxCents(int $taxCents);

    /**
     * Get Tax in Cents
     * @return int
     */
    public function getTaxCents();

    /**
     * Set Total in cents
     * @param int $totalCents
     * @return \Simpler\Checkout\Api\Data\ShippingOptionInterface
     */
    public function setTotalCents(int $totalCents);

    /**
     * Get Total in Cents
     * @return int
     */
    public function getTotalCents();

    /**
     * Set shipping option type
     * @param string $type
     * @return \Simpler\Checkout\Api\Data\ShippingOptionInterface
     */
    public function setType(string $type);

    /**
     * Get shipping option type
     * @return string
     */
    public function getType();
}
