<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface AddressInterface
{
    /**
     * Fax.
     */
    const FAX = 'fax';

    /**
     * Region.
     */
    const REGION = 'region';

    /**
     * Postal code.
     */
    const POSTCODE = 'postcode';

    /**
     * Last name.
     */
    const LASTNAME = 'lastname';

    /**
     * Street.
     */
    const STREET = 'street';

    /**
     * City.
     */
    const CITY = 'city';

    /**
     * Email address.
     */
    const EMAIL = 'email';

    /**
     * Telephone number.
     */
    const TELEPHONE = 'telephone';

    /**
     * Country ID.
     */
    const COUNTRY_ID = 'country_id';

    /**
     * First name.
     */
    const FIRSTNAME = 'firstname';

    /**
     * Address type.
     */
    const ADDRESS_TYPE = 'address_type';

    /**
     * Prefix.
     */
    const PREFIX = 'prefix';

    /**
     * Company.
     */
    const COMPANY = 'company';
    /**
     * Notes
     */
    const NOTES = "notes";

    /**
     * Notes
     */
    const PICKUP_STORE_ID = "pickup_store_id";

    const ATTRIBUTES = [
        self::FAX,
        self::REGION,
        self::POSTCODE,
        self::LASTNAME,
        self::FIRSTNAME,
        self::STREET,
        self::CITY,
        self::EMAIL,
        self::TELEPHONE,
        self::COUNTRY_ID,
        self::PICKUP_STORE_ID
    ];

    /**
     * Set firstname
     * @param string $firstname
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setFirstname(string $firstname);

    /**
     * Set lastname
     * @param string $lastname
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setLastname(string $lastname);

    /**
     * Set phone
     * @param string $phone
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setTelephone(string $phone);

    /**
     * Set Street
     * @param string $street
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setStreet(string $street);

    /**
     * Set city
     * @param string $city
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setCity($city);

    /**
     * Set country
     * @param string $country
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setCountryId($country);

    /**
     * Set postcode
     * @param string $postcode
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setPostcode($postcode);

    /**
     * Set notes
     * @param string $notes
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setNotes($notes);

    /**
     * Set region
     * @param string $region
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setRegion($region);

    /**
     * Set pickup store id
     * @param string $storeid
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function setPickupStoreId($storeid);

    /**
     * Get firstname
     * @return string
     */
    public function getFirstname();

    /**
     * Get lastname
     * @return string
     */
    public function getLastname();

    /**
     * Get phone
     * @return string
     */
    public function getTelephone();

    /**
     * Get street
     * @return string
     */
    public function getStreet();

    /**
     * Get city
     * @return string
     */
    public function getCity();

    /**
     * Get postcode
     * @return string
     */
    public function getPostcode();

    /**
     * Get country
     * @return string
     */
    public function getCountryId();

    /**
     * Get notes
     * @return string
     */
    public function getNotes();

    /**
     * Get region
     * @return string
     */
    public function getRegion();

    /**
     * Get pickup store id
     * @return string
     */
    public function getPickupStoreId();
    /**
     * Convert address to Magento\Quote\Model\Quote\Address attributes
     *
     * @return array
     */
    public function toMagentoQuoteAddressArray(): array;

    /**
     * Check if address is empty
     * @return bool
     */
    public function isEmpty();
}
