<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface ConfigurableOptionInterface
{
    const CONFIGURABLE_OPTION_ID = "configurable_option_id";
    const CONFIGURABLE_OPTION_LABEL = "configurable_option_label";
    const CONFIGURABLE_OPTION_VALUE_ID = "configurable_option_value_id";
    const CONFIGURABLE_OPTION_VALUE_LABEL = "configurable_option_value_label";


    /**
     * Set option id
     * @param string $optionId
     * @return \Simpler\Checkout\Api\Data\ConfigurableOptionInterface
     */
    public function setConfigurableOptionId(string $optionId);

    /**
     * Get option id
     * @return string
     */
    public function getConfigurableOptionId(): string;

     /**
     * Set option label
     * @param string $optionLabel
     * @return \Simpler\Checkout\Api\Data\ConfigurableOptionInterface
     */
    public function setConfigurableOptionLabel(string $optionLabel);

    /**
     * Get option label
     * @return string
     */
    public function getConfigurableOptionLabel(): string;

    /**
     * Set value id
     * @param string $valueId
     * @return \Simpler\Checkout\Api\Data\ConfigurableOptionInterface
     */
    public function setConfigurableOptionValueId(string $valueId);

    /**
     * Get value id
     * @return string
     */
    public function getConfigurableOptionValueId(): string;

     /**
     * Set value label
     * @param string $valueLabel
     * @return \Simpler\Checkout\Api\Data\ConfigurableOptionInterface
     */
    public function setConfigurableOptionValueLabel(string $valueLabel);

    /**
     * Get value label
     * @return string
     */
    public function getConfigurableOptionValueLabel(): string;
}
