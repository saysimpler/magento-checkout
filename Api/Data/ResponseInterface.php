<?php

namespace Simpler\Checkout\Api\Data;

interface ResponseInterface
{

    const REQUEST_ID = "request_id";
    const STORE_BASE_URL = "store_base_url";

    /**
     * Set request id
     *
     * @param  string  $requestId
     * @return \Simpler\Checkout\Api\Data\ResponseInterface
     */
    public function setRequestId(string $requestId): ResponseInterface;

    /**
     * Get request id
     *
     * @return string
     */
    public function getRequestId();

    /**
     * Set Store Base Url
     * @param string $storeBaseUrl
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setStoreBaseUrl(string $storeBaseUrl);

    /**
     * Get Store Base Url
     * @return string
     */
    public function getStoreBaseUrl();
}
