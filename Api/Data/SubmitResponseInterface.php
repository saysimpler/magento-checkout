<?php

namespace Simpler\Checkout\Api\Data;

interface SubmitResponseInterface extends ResponseInterface
{
    const EXTERNAL_ID = "external_id";

    /**
     * Set Magento Order Id
     *
     * @param string $externalId
     * @return \Simpler\Checkout\Api\Data\SubmitResponseInterface
     */
    public function setExternalId(string $externalId);

    /**
     * Get Magento Order Id
     * @return string
     */
    public function getExternalId();
}
