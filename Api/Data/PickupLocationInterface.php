<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface PickupLocationInterface
{
    const NAME = "name";
    const ID   = "id";


    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param  string  $id
     * @return \Simpler\Checkout\Api\Data\PickupLocationInterface
     */
    public function setId(string $id);

    /**
     * @param  string  $name
     * @return \Simpler\Checkout\Api\Data\PickupLocationInterface
     */
    public function setName(string $name);
}
