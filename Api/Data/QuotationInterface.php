<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface QuotationInterface
{
    const CURRENCY = "currency";
    const ITEMS = "items";
    const SHIPPING_TO = "shipping_to";
    const COUPON = "coupon";
    const EMAIL = "email";

    /**
     * Set Currency Code
     * @param string $currency
     * @return \Simpler\Checkout\Api\Data\QuotationInterface
     */
    public function setCurrency(string $currency);

    /**
     * Get Currency Code
     * @return string
     */
    public function getCurrency();

    /**
     * Set Items
     * @param \Simpler\Checkout\Api\Data\ItemInterface[] $items
     * @return \Simpler\Checkout\Api\Data\QuotationInterface
     */
    public function setItems(array $items);

    /**
     * Get Items
     * @return \Simpler\Checkout\Api\Data\ItemInterface[]
     */
    public function getItems();

    /**
     * Set Shipping To
     * @param ?\Simpler\Checkout\Api\Data\AddressInterface $shippingTo
     * @return \Simpler\Checkout\Api\Data\QuotationInterface
     */
    public function setShippingTo(?\Simpler\Checkout\Api\Data\AddressInterface $shippingTo);


    /**
     * Get Shipping To
     * @return ?\Simpler\Checkout\Api\Data\AddressInterface
     */
    public function getShippingTo(): ?\Simpler\Checkout\Api\Data\AddressInterface;

    /**
     * Set Coupon
     * @param string $coupon
     * @return \Simpler\Checkout\Api\Data\QuotationInterface
     */
    public function setCoupon(string $coupon);

    /**
     * Get Coupon
     * @return string
     */
    public function getCoupon();

    /**
     * Set email
     * @param string $email
     * @return \Simpler\Checkout\Api\Data\QuotationInterface
     */
    public function setEmail(string $email);

    /**
     * Get email
     * @return string
     */
    public function getEmail();
}
