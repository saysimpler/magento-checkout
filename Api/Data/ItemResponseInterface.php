<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface ItemResponseInterface extends ItemInterface
{

    const TITLE = "title";
    const PRICE = "price";
    const SKU = "sku";
    const IMAGE_URL = "image_url";
    const TAX_CENTS = "tax_cents";
    const SHIPPABLE = "shippable";
    const CONFIGURATIONS = "configurations";
    const TYPE  = "type";
    const CHILDREN = "children";

    /**
     * Set title
     * @param string $title
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setTitle(string $title);

    /**
     * Get title
     * @return string
     */
    public function getTitle();

    /**
     * Set price
     * @param float $price
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setPrice(float $price);

    /**
     * Get price
     * @return float
     */
    public function getPrice();


    /**
     * Set Sku
     * @param string $sku
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setSku(string $sku);

    /**
     * Get sku
     * @return string
     */
    public function getSku();

    /**
     * Set Image Url
     * @param string $imageUrl
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setImageUrl(string $imageUrl);

    /**
     * Get Image Url
     * @return string
     */
    public function getImageUrl();

    /**
     * Set tax cents
     * @param int $taxCents
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setTaxCents(int $taxCents);

    /**
     * Get tax cents
     * @return int
     */
    public function getTaxCents();

    /**
     * Set shippable
     * @param bool $shippable
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setShippable(bool $shippable);

    /**
     * Get shippable
     * @return bool
     */
    public function getShippable();

    /**
     * Set Configurations
     * @param  \Simpler\Checkout\Api\Data\ConfigurableOptionInterface[]  $configurations
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setConfigurations(array $configurations);

     /**
     * Get Configurations
     * @return \Simpler\Checkout\Api\Data\ConfigurableOptionInterface[]
     */
    public function getConfigurations();

    /**
     * Set Product Type
     * @param string $type
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setType(string $type);

    /**
     * Get Product Type
     * @return string
     */
    public function getType();

    /**
     * Set Children
     * @param  \Simpler\Checkout\Api\Data\ItemResponseInterface[]  $children
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface
     */
    public function setChildren(array $children);

     /**
     * Get Children
     * @return \Simpler\Checkout\Api\Data\ItemResponseInterface[]
     */
    public function getChildren();
    
}
