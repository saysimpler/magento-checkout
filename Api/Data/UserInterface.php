<?php

namespace Simpler\Checkout\Api\Data;

interface UserInterface
{
    const FIRSTNAME = "firstName";
    const LASTNAME = "lastName";
    const EMAIL = "email";

    /**
     * Set First Name
     * @param string $firstName
     * @return \Simpler\Checkout\Api\Data\UserInterface
     */
    public function setFirstName(string $firstName);

    /**
     * Get First Name
     * @return string
     */
    public function getFirstName();

    /**
     * Set Last Name
     * @param string $lastName
     * @return \Simpler\Checkout\Api\Data\UserInterface
     */
    public function setLastName(string $lastName);

    /**
     * Get Last Name
     * @return string
     */
    public function getLastName();

    /**
     * Set Email
     * @param string $email
     * @return \Simpler\Checkout\Api\Data\UserInterface
     */
    public function setEmail(string $email);

    /**
     * Get Email
     * @return string
     */
    public function getEmail();
}
