<?php

namespace Simpler\Checkout\Api\Data;

interface ProductsRequestInterface extends RequestInterface
{

    const ITEMS = "items";

    /**
     * Set Items
     *
     * @param  \Simpler\Checkout\Api\Data\ItemInterface[]  $items
     * @return \Simpler\Checkout\Api\Data\ProductsRequestInterface
     */
    public function setItems(array $items);

    /**
     * Get Items
     *
     * @return \Simpler\Checkout\Api\Data\ItemInterface[]
     */
    public function getItems();
}
