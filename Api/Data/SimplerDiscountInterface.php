<?php

namespace Simpler\Checkout\Api\Data;

interface SimplerDiscountInterface
{
    const AMOUNT = "amount";

    /**
     * Set amount.
     *
     * @param  float  $amount
     * @return \Simpler\Checkout\Api\Data\SimplerDiscountInterface
     */
    public function setAmount(float $amount);

    /**
     * Get amount.
     *
     * @return float
     */
    public function getAmount(): float;
}
