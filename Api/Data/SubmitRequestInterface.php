<?php

namespace Simpler\Checkout\Api\Data;

interface SubmitRequestInterface extends RequestInterface
{
    const SIMPLER_ORDER_ID = "simpler_order_id";
    const USER             = "user";
    const NOTE             = "note";
    const CURRENCY         = "currency";
    const ITEMS            = "items";
    const SHIPPING_TO      = "shipping_to";
    const COUPON           = "coupon";
    const SHIPPING_METHOD  = "shipping_method";
    const PAYMENT_METHOD   = "payment_method";
    const SIMPLER_DISCOUNT = "simpler_discount";
    const INVOICE          = 'invoice';
    const BOXNOW          = 'box_now';

    /**
     * Set Currency Code
     *
     * @param  string  $currency
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setCurrency(string $currency);

    /**
     * Get Currency Code
     *
     * @return string
     */
    public function getCurrency();

    /**
     * Set Note
     *
     * @param  string  $note
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setNote(string $note);

    /**
     * Get Note
     *
     * @return string
     */
    public function getNote();

    /**
     * Get user
     *
     * @return \Simpler\Checkout\Api\Data\UserInterface
     */
    public function getUser();

    /**
     * Set User
     *
     * @param  \Simpler\Checkout\Api\Data\UserInterface
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setUser(UserInterface $user);

    /**
     * Set Items
     *
     * @param  \Simpler\Checkout\Api\Data\ItemInterface[]  $items
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setItems(array $items);

    /**
     * Get Items
     *
     * @return \Simpler\Checkout\Api\Data\ItemInterface[]
     */
    public function getItems();

    /**
     * Set Shipping To
     *
     * @param  \Simpler\Checkout\Api\Data\AddressInterface  $shippingTo
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setShippingTo(AddressInterface $sippingTo);


    /**
     * Get Shipping To
     *
     * @return \Simpler\Checkout\Api\Data\AddressInterface
     */
    public function getShippingTo();

    /**
     * Set Coupon
     *
     * @param  string  $coupon
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setCoupon(string $coupon);

    /**
     * Get Coupon
     *
     * @return string
     */
    public function getCoupon();

    /**
     * Set Shipping Method
     *
     * @param  string  $shippingMethod
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setShippingMethod(string $shippingMethod);

    /**
     * Get Shipping Method
     *
     * @return string
     */
    public function getShippingMethod();

    /**
     * Set Payment Method
     *
     * @param  string|null  $shippingMethod
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setPaymentMethod(string $paymentMethod);

    /**
     * Get Payment Method
     *
     * @return string|null
     */
    public function getPaymentMethod();

    /**
     * Set Simpler Discount
     *
     * @param  \Simpler\Checkout\Api\Data\SimplerDiscountInterface  $simplerDiscount
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setSimplerDiscount(SimplerDiscountInterface $simplerDiscount);

    /**
     * Get Simpler Discount
     *
     * @return \Simpler\Checkout\Api\Data\SimplerDiscountInterface
     */
    public function getSimplerDiscount();

    /**
     * Set Shipping Method
     *
     * @param  string  $simplerOrderId
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setSimplerOrderId(string $simplerOrderId);

    /**
     * Get Simpler Order Id
     *
     * @return string
     */
    public function getSimplerOrderId();

    /**
     * @param  \Simpler\Checkout\Api\Data\OrderInvoiceInterface  $invoice
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setInvoice(OrderInvoiceInterface $invoice);

    /**
     * @return \Simpler\Checkout\Api\Data\OrderInvoiceInterface
     */
    public function getInvoice();

    /**
     * @param  \Simpler\Checkout\Api\Data\OrderBoxNowInterface  $boxNow
     * @return \Simpler\Checkout\Api\Data\SubmitRequestInterface
     */
    public function setBoxNow(OrderBoxNowInterface $boxNow);

    /**
     * @return \Simpler\Checkout\Api\Data\OrderBoxNowInterface
     */
    public function getBoxNow();
}
