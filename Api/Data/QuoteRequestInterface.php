<?php

namespace Simpler\Checkout\Api\Data;

interface QuoteRequestInterface extends RequestInterface
{
    const QUOTATION = "quotation";

    /**
     * Set quotation
     *
     * @param  \Simpler\Checkout\Api\Data\QuotationInterface  $quotation
     * @return \Simpler\Checkout\Api\Data\QuoteRequestInterface
     */
    public function setQuotation(\Simpler\Checkout\Api\Data\QuotationInterface $quotation);

    /**
     * Get quotation
     *
     * @return \Simpler\Checkout\Api\Data\QuotationInterface
     */
    public function getQuotation();
}
