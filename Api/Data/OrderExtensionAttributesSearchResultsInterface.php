<?php

namespace Simpler\Checkout\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface OrderExtensionAttributesSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return OrderExtensionAttributesInterface[]
     */
    public function getItems();

    /**
     * @param OrderExtensionAttributesInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
