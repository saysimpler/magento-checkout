<?php

namespace Simpler\Checkout\Api\Data;

interface OrderInvoiceInterface
{
    const COMPANY_NAME       = 'company_name';
    const Activity     = 'activity';
    const TAX_ID        = 'tax_id';
    const TAX_AUTHORITY = 'tax_authority';
    const COMPANY_ADDRESS    = 'company_address';

    /**
     * @return string
     */
    public function getCompanyName();

    /**
     * @param  string  $name
     * @return \Simpler\Checkout\Api\Data\OrderInvoiceInterface
     */
    public function setCompanyName(string $name);

    /**
     * @return string
     */
    public function getActivity();

    /**
     * @param  string  $activity
     * @return \Simpler\Checkout\Api\Data\OrderInvoiceInterface
     */
    public function setActivity(string $activity);

    /**
     * @return string
     */
    public function getTaxId();

    /**
     * @param  string  $taxid
     * @return \Simpler\Checkout\Api\Data\OrderInvoiceInterface
     */
    public function setTaxId(string $taxid);

    /**
     * @return string
     */
    public function getTaxAuthority();

    /**
     * @param  string  $taxAuthority
     * @return \Simpler\Checkout\Api\Data\OrderInvoiceInterface
     */
    public function setTaxAuthority(string $taxAuthority);

    /**
     * @return string
     */
    public function getCompanyAddress();

    /**
     * @param  string  $Address
     * @return \Simpler\Checkout\Api\Data\OrderInvoiceInterface
     */
    public function setCompanyAddress(string $Address);
}
