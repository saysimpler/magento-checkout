<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface ItemInterface
{
    const ID = "id";
    const QUANTITY = "quantity";
    const TOTAL_CENTS = "total_cents";
    const CUSTOM_VALUES = "custom_values";

    /**
     * Set item id
     * @param string $id
     * @return \Simpler\Checkout\Api\Data\ItemInterface
     */
    public function setId(string $id);

    /**
     * Get item id
     * @return string
     */
    public function getId(): string;

    /**
     * Set item quantity
     * @param int $quantity
     * @return \Simpler\Checkout\Api\Data\ItemInterface
     */
    public function setQuantity(int $quantity);

    /**
     * Get item quantity
     * @return int
     */
    public function getQuantity();

    /**
     * Set total cents
     * @param int $totalCents
     * @return \Simpler\Checkout\Api\Data\ItemInterface
     */
    public function setTotalCents(int $totalCents);

    /**
     * Get total cents
     * @return int
     */
    public function getTotalCents();

    /**
     * Set custom values
     * @param \Simpler\Checkout\Api\Data\ItemCustomValuesInterface[] $customValues
     * @return \Simpler\Checkout\Api\Data\ItemInterface
     */
    public function setCustomValues(array $customValues);

    /**
     * Get custom values
     * @return \Simpler\Checkout\Api\Data\ItemCustomValuesInterface[]
     */
    public function getCustomValues();

}
