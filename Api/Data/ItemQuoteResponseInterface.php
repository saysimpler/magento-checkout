<?php

namespace Simpler\Checkout\Api\Data;

/**
 * @api
 */
interface ItemQuoteResponseInterface
{
    const CURRENCY = "currency";
    const ITEMS = "items";
    const SHIPPING_OPTION = "shipping_option";
    const PAYMENT_METHODS = "payment_methods";
    const TOTAL_CENTS = "total_cents";
    const DISCOUNT_CENTS = "discount_cents";
    const FEES = "fees";

    /**
     * Set Currency Code
     * @param string $currency
     * @return \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface
     */
    public function setCurrency(string $currency);

    /**
     * Get Currency Code
     * @return string
     */
    public function getCurrency();

    /**
     * Set Items
     * @param \Simpler\Checkout\Api\Data\ItemInterface[] $items
     * @return \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface
     */
    public function setItems(array $items);

    /**
     * Get Items
     * @return \Simpler\Checkout\Api\Data\ItemInterface[]
     */
    public function getItems();

    /**
     * Set Shipping Option
     * @param ?\Simpler\Checkout\Api\Data\ShippingOptionInterface $shippingOption
     * @return  \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface
     */
    public function setShippingOption(?\Simpler\Checkout\Api\Data\ShippingOptionInterface $shippingOption);

    /**
     * Get Shipping Option
     * @return ?\Simpler\Checkout\Api\Data\ShippingOptionInterface
     */
    public function getShippingOption(): ?\Simpler\Checkout\Api\Data\ShippingOptionInterface;

    /**
     * Set payment methods
     * @param \Simpler\Checkout\Api\Data\PaymentMethodInterface[] $methods
     * @return \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface
     */
    public function setPaymentMethods(array $methods);

    /**
     * Get payment methods
     * @return \Simpler\Checkout\Api\Data\PaymentMethodInterface[]
     */
    public function getPaymentMethods();

    /**
     * Set total cents
     * @param int $totalCents
     * @return \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface
     */
    public function setTotalCents(int $totalCents);

    /**
     * Get total cents
     * @return int
     */
    public function getTotalCents();

    /**
     * Set discount cents
     * @param int $discountCents
     * @return \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface
     */
    public function setDiscountCents(int $discountCents);

    /**
     * Get discount cents
     * @return int
     */
    public function getDiscountCents();

    /**
     * Add a fee entry
     * @param string $title
     * @param \Simpler\Checkout\Api\Data\FeeInterface $fee
     * @return \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface
     */
    public function addFee(\Simpler\Checkout\Api\Data\FeeInterface $fee);

    /**
     * Set fees
     * @param \Simpler\Checkout\Api\Data\FeeInterface[] $fees
     * @return \Simpler\Checkout\Api\Data\ItemQuoteResponseInterface
     */
    public function setFees(array $fees);

    /**
     * Get discounts
     * @return \Simpler\Checkout\Api\Data\FeeInterface[]
     */
    public function getFees();
}
