<?php

namespace Simpler\Checkout\Api\About;

/**
 * @api
 */
interface AboutInterface
{

    const Db_Version = "db_version";
    const PHPInfo = "php_info";
    const Magento = "magento";

    /**
     * Set DB version
     * @param string $dbversion
     * @return \Simpler\Checkout\Api\About\AboutInterface
     */
    public function setDbVersion($dbversion);

    /**
     * Get DB version
     * @return string
     */
    public function getDbVersion();

    /**
     * Set PHPInfo
     * @param \Simpler\Checkout\Api\About\PhpInfoInterface $phpInfo
     * @return \Simpler\Checkout\Api\About\AboutInterface
     */
    public function setPhpInfo($phpInfo);

    /**
     * Get PHPInfo
     * @return \Simpler\Checkout\Api\About\PhpInfoInterface
     */
    public function getPhpInfo();

    /**
     * Set Magento
     * @param \Simpler\Checkout\Api\About\MagentoInfoInterface $magentoInfo
     * @return \Simpler\Checkout\Api\About\AboutInterface
     */
    public function setMagento($magentoInfo);

    /**
     * Get Magento
     * @return \Simpler\Checkout\Api\About\MagentoInfoInterface
     */
    public function getMagento();
}
