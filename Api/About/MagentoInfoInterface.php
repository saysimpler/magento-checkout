<?php

namespace Simpler\Checkout\Api\About;

/**
 * @api
 */
interface MagentoInfoInterface
{

    const VERSION = "version";
    const MODULES = "modules";

    /**
     * Set version
     * @param string $version
     * @return \Simpler\Checkout\Api\About\MagentoInfoInterface
     */
    public function setVersion(string $version);

    /**
     * Get version
     * @return string
     */
    public function getVersion();

    /**
     * Set modules
     * @param \Simpler\Checkout\Api\Json\AssociativeArrayItemInterface[] $modules
     * @return \Simpler\Checkout\Api\About\MagentoInfoInterface
     */
    public function setModules($modules);

    /**
     * Get modules
     * @return \Simpler\Checkout\Api\Json\AssociativeArrayItemInterface[]
     */
    public function getModules();
}
