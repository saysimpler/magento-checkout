<?php

namespace Simpler\Checkout\Api\About;

/**
 * @api
 */
interface PhpInfoInterface
{

    const VERSION = "version";
    const EXTENSIONS = "extensions";

    /**
     * Set version
     * @param string $version
     * @return \Simpler\Checkout\Api\About\PhpInfoInterface
     */
    public function setVersion(string $version);

    /**
     * Get version
     * @return string
     */
    public function getVersion();

    /**
     * Set extensions
     * @param mixed $extensions
     * @return \Simpler\Checkout\Api\About\PhpInfoInterface
     */
    public function setExtensions($extensions);

    /**
     * Get extensions
     * @return mixed
     */
    public function getExtensions();

}
