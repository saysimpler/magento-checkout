<?php

namespace Simpler\Checkout\Api;

interface SimplerApiInterface
{
    /**
     * Create Simpler QuoteRequest
     *
     * @param  \Simpler\Checkout\Api\Data\QuoteRequestInterface  $cart
     * @return \Simpler\Checkout\Api\Data\QuoteResponseInterface
     * @throws \Simpler\Checkout\Exception\UnavailableProductException
     * @throws \Simpler\Checkout\Exception\UnshippableLocationException
     * @throws \Simpler\Checkout\Exception\MissingQuotationException
     * @throws \Simpler\Checkout\Exception\InvalidProductException
     * @throws \Simpler\Checkout\Exception\OutOfStockProductException
     */
    public function quote(\Simpler\Checkout\Api\Data\QuoteRequestInterface $cart);


    /**
     * Get Products Data
     *
     * @param  \Simpler\Checkout\Api\Data\ProductsRequestInterface  $products
     * @return \Simpler\Checkout\Api\Data\ProductsResponseInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function products(\Simpler\Checkout\Api\Data\ProductsRequestInterface $products);

    /**
     * Submit Order
     *
     * @param  \Simpler\Checkout\Api\Data\SubmitRequestInterface  $order
     * @return \Simpler\Checkout\Api\Data\SubmitResponseInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Simpler\Checkout\Exception\InvalidProductException
     * @throws \Simpler\Checkout\Exception\OutOfStockProductException
     * @throws \Simpler\Checkout\Exception\UnavailableProductException
     */
    public function submit(\Simpler\Checkout\Api\Data\SubmitRequestInterface $order);

    /**
     * Gets Magento installation information
     *
     * @return \Simpler\Checkout\Api\About\AboutInterface
     */
    public function about();


    /**
     * Get available order pickup locations.
     *
     * @return \Simpler\Checkout\Api\Data\PickupLocationInterface[]
     */
    public function pickupLocations();
}
