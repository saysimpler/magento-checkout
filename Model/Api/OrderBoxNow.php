<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\OrderBoxNowInterface;

class OrderBoxNow extends DataObject implements OrderBoxNowInterface
{

    public function getLockerId()
    {
        return $this->getData(OrderBoxNowInterface::LOCKER_ID);
    }

    public function setLockerId(string $lockerId)
    {
        return $this->setData(OrderBoxNowInterface::LOCKER_ID, $lockerId);
    }

    public function getAddress()
    {
        return $this->getData(OrderBoxNowInterface::ADDRESS);
    }

    public function setAddress(string $address)
    {
        return $this->setData(OrderBoxNowInterface::ADDRESS, $address);
    }

    public function getPostcode()
    {
        return $this->getData(OrderBoxNowInterface::POSTCODE);
    }

    public function setPostcode(string $postcode)
    {
        return $this->setData(OrderBoxNowInterface::POSTCODE, $postcode);
    }
}
