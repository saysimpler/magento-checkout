<?php

namespace Simpler\Checkout\Model\Api;

use Simpler\Checkout\Api\Data\SubmitResponseInterface;

class SubmitResponse extends Response implements SubmitResponseInterface
{

    /**
     * @inheritDoc
     */
    public function setExternalId(string $externalId): SubmitResponseInterface
    {
        return $this->setData(SubmitResponseInterface::EXTERNAL_ID, $externalId);
    }

    /**
     * @inheritDoc
     */
    public function getExternalId()
    {
        return $this->getData(SubmitResponseInterface::EXTERNAL_ID);
    }
}
