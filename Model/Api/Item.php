<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\ItemInterface;
use Simpler\Checkout\Api\Data\ItemResponseInterface;

class Item extends DataObject implements ItemInterface, ItemResponseInterface
{

    /**
     * @inheritDoc
     */
    public function setId(string $id)
    {
        return $this->setData(ItemInterface::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->getData(ItemInterface::ID);
    }

    /**
     * @inheritDoc
     */
    public function setQuantity(int $quantity)
    {
        return $this->setData(ItemInterface::QUANTITY, $quantity);
    }

    /**
     * @inheritDoc
     */
    public function getQuantity()
    {
        return $this->getData(ItemInterface::QUANTITY);
    }

    /**
     * @inheritDoc
     */
    public function setCustomValues(array $customValues)
    {
        return $this->setData(ItemInterface::CUSTOM_VALUES, $customValues);
    }

    /**
     * @inheritDoc
     */
    public function getCustomValues()
    {
        return $this->getData(ItemInterface::CUSTOM_VALUES);
    }

    public function setTitle(string $title)
    {
        return $this->setData(ItemResponseInterface::TITLE, $title);
    }

    public function getTitle()
    {
        return $this->getData(ItemResponseInterface::TITLE);
    }

    public function setPrice(float $price)
    {
        return $this->setData(ItemResponseInterface::PRICE, $price);
    }

    public function getPrice()
    {
        return $this->getData(ItemResponseInterface::PRICE);
    }

    public function setSku(string $sku)
    {
        return $this->setData(ItemResponseInterface::SKU, $sku);
    }

    public function getSku()
    {
        return $this->getData(ItemResponseInterface::SKU);
    }

    public function setImageUrl(string $imageUrl)
    {
        return $this->setData(ItemResponseInterface::IMAGE_URL, $imageUrl);
    }

    public function getImageUrl()
    {
        return $this->getData(ItemResponseInterface::IMAGE_URL);
    }

    public function setShippable(bool $shippable)
    {
        return $this->setData(ItemResponseInterface::SHIPPABLE, $shippable);
    }

    public function getShippable()
    {
        return $this->getData(ItemResponseInterface::SHIPPABLE);
    }

    public function setTaxCents(int $taxCents)
    {
        return $this->setData(ItemResponseInterface::TAX_CENTS, $taxCents);
    }

    public function getTaxCents()
    {
        return $this->getData(ItemResponseInterface::TAX_CENTS);
    }

    public function setTotalCents(int $totalCents)
    {
        return $this->setData(ItemInterface::TOTAL_CENTS, $totalCents);
    }

    public function getTotalCents()
    {
        return $this->getData(ItemInterface::TOTAL_CENTS);
    }

    public function setConfigurations(array $configurations)
    {
        return $this->setData(ItemResponseInterface::CONFIGURATIONS, $configurations);
    }

    public function getConfigurations()
    {
        return $this->getData(ItemResponseInterface::CONFIGURATIONS);
    }

    public function setChildren(array $children)
    {
        return $this->setData(ItemResponseInterface::CHILDREN, $children);
    }

    public function getChildren()
    {
        return $this->getData(ItemResponseInterface::CHILDREN);
    }

    public function setType(string $type)
    {
        return $this->setData(ItemResponseInterface::TYPE, $type);
    }

    public function getType()
    {
        return $this->getData(ItemResponseInterface::TYPE);
    }
    
}
