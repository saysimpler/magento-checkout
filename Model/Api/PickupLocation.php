<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\PickupLocationInterface;

class PickupLocation extends DataObject implements PickupLocationInterface
{

    /** @inheritdoc */
    public function getId(): string
    {
        return $this->getData(PickupLocationInterface::ID);
    }

    /** @inheritdoc */
    public function getName(): string
    {
        return $this->getData(PickupLocationInterface::NAME);
    }

    /** @inheritdoc */
    public function setId(string $id)
    {
        return $this->setData(PickupLocationInterface::ID, $id);

    }

    /** @inheritdoc */
    public function setName(string $name)
    {
        return $this->setData(PickupLocationInterface::NAME, $name);
    }
}
