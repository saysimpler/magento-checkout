<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\ShippingOptionInterface;

class ShippingOption extends DataObject implements ShippingOptionInterface
{

    /**
     * @inheritDoc
     */
    public function setId(string $id)
    {
        return $this->setData(ShippingOptionInterface::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(ShippingOptionInterface::ID);
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name)
    {
        return $this->setData(ShippingOptionInterface::NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getData(ShippingOptionInterface::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setCostCents(int $costCents)
    {
        return $this->setData(ShippingOptionInterface::COST_CENTS, $costCents);
    }

    /**
     * @inheritDoc
     */
    public function getCostCents()
    {
        return $this->getData(ShippingOptionInterface::COST_CENTS);
    }

    /**
     * @inheritDoc
     */
    public function setTaxCents(int $taxCents)
    {
        return $this->setData(ShippingOptionInterface::TAX_CENTS, $taxCents);
    }

    /**
     * @inheritDoc
     */
    public function getTaxCents()
    {
        return $this->getData(ShippingOptionInterface::TAX_CENTS);
    }

    /**
     * @inheritDoc
     */
    public function setTotalCents(int $totalCents)
    {
        return $this->setData(ShippingOptionInterface::TOTAL_CENTS, $totalCents);
    }

    /**
     * @inheritDoc
     */
    public function getTotalCents()
    {
        return $this->getData(ShippingOptionInterface::TOTAL_CENTS);
    }

    /**
     * @inheritdoc
     */
    public function setType(string $type)
    {
        return $this->setData(ShippingOptionInterface::TYPE, $type);
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->getData(ShippingOptionInterface::TYPE);
    }
}
