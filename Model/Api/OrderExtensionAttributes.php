<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\Model\AbstractModel;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface;
use Simpler\Checkout\Api\Data\OrderInvoiceInterface;

class OrderExtensionAttributes extends AbstractModel implements OrderExtensionAttributesInterface
{
    protected function _construct()
    {
        $this->_init(\Simpler\Checkout\Model\ResourceModel\OrderExtensionAttributesResource::class);
    }

    public function getId()
    {
        return $this->getData(OrderExtensionAttributesInterface::ID);

    }

    public function setId($id)
    {
        return $this->setData(OrderExtensionAttributesInterface::ID, $id);
    }

    public function getOrderId()
    {
        return $this->getData(OrderExtensionAttributesInterface::ORDER_ID);

    }

    public function setOrderId($orderId)
    {
        return $this->setData(OrderExtensionAttributesInterface::ORDER_ID, $orderId);
    }

    public function getSimplerOrderId()
    {
        return $this->getData(OrderExtensionAttributesInterface::SIMPLER_ORDER_ID);

    }

    public function setSimplerOrderId(string $simplerOrderId)
    {
        return $this->setData(OrderExtensionAttributesInterface::SIMPLER_ORDER_ID, $simplerOrderId);
    }

    public function getInvoice()
    {
        return $this->getData(OrderExtensionAttributesInterface::INVOICE);
    }

    public function setInvoice(OrderInvoiceInterface $invoice)
    {
        return $this->setData(OrderExtensionAttributesInterface::INVOICE, $invoice);
    }
}
