<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\ConfigurableOptionInterface;

class ConfigurableOption extends DataObject implements ConfigurableOptionInterface
{


    /**
     * @inheritDoc
     */
    public function setConfigurableOptionId(string $optionId)
    {
        return $this->setData(ConfigurableOptionInterface::CONFIGURABLE_OPTION_ID, $optionId);
    }

   /**
     * @inheritDoc
     */
    public function getConfigurableOptionId(): string
    {
        return $this->getData(ConfigurableOptionInterface::CONFIGURABLE_OPTION_ID);
    }

      /**
     * @inheritDoc
     */
    public function setConfigurableOptionLabel(string $optionLabel)
    {
        return $this->setData(ConfigurableOptionInterface::CONFIGURABLE_OPTION_LABEL, $optionLabel);
    }

     /**
     * @inheritDoc
     */
    public function getConfigurableOptionLabel(): string
    {
        return $this->getData(ConfigurableOptionInterface::CONFIGURABLE_OPTION_LABEL);
    }

     /**
     * @inheritDoc
     */
    public function setConfigurableOptionValueId(string $valueId)
    {
        return $this->setData(ConfigurableOptionInterface::CONFIGURABLE_OPTION_VALUE_ID, $valueId);
    }

     /**
     * @inheritDoc
     */
    public function getConfigurableOptionValueId(): string
    {
        return $this->getData(ConfigurableOptionInterface::CONFIGURABLE_OPTION_VALUE_ID);
    }

      /**
     * @inheritDoc
     */
    public function setConfigurableOptionValueLabel(string $valueLabel)
    {
        return $this->setData(ConfigurableOptionInterface::CONFIGURABLE_OPTION_VALUE_LABEL, $valueLabel);
    }

     /**
     * @inheritDoc
     */
    public function getConfigurableOptionValueLabel(): string
    {
        return $this->getData(ConfigurableOptionInterface::CONFIGURABLE_OPTION_VALUE_LABEL);
    }

}
