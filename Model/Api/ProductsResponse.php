<?php

namespace Simpler\Checkout\Model\Api;

use Simpler\Checkout\Api\Data\ItemResponseInterface;
use Simpler\Checkout\Api\Data\ProductsResponseInterface;

class ProductsResponse extends Response implements ProductsResponseInterface
{
    protected $_items = [];

    /**
     * @inheritDoc
     */
    public function setItems(array $items)
    {
        return $this->setData(ProductsResponseInterface::ITEMS, $items);
    }

    /**
     * @inheritDoc
     */
    public function getItems()
    {
        return $this->getData(ProductsResponseInterface::ITEMS);
    }

    /**
     * @param  ItemResponseInterface  $item
     * @return $this
     */
    public function addItem(ItemResponseInterface $item): ProductsResponse
    {
        $this->_items[] = $item;
        $this->setItems($this->_items);

        return $this;
    }

}
