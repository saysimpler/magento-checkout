<?php

namespace Simpler\Checkout\Model\Api;

use Simpler\Checkout\Api\Data\ProductsRequestInterface;

class ProductsRequest extends Request implements ProductsRequestInterface
{
    /**
     * @inheritDoc
     */
    public function setItems(array $items)
    {
        return $this->setData(ProductsRequestInterface::ITEMS, $items);
    }

    /**
     * @inheritDoc
     */
    public function getItems()
    {
        return $this->getData(ProductsRequestInterface::ITEMS);
    }
}
