<?php

namespace Simpler\Checkout\Model\Api\Cart;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\AddressInterface;
use Simpler\Checkout\Api\Data\QuotationInterface;
use Simpler\Checkout\Api\Data\ShippingOptionInterface;

class Quotation extends DataObject implements QuotationInterface
{
    /**
     * @inheritdoc
     */
    public function setCurrency(string $currency)
    {
        return $this->setData(QuotationInterface::CURRENCY, $currency);
    }

    /**
     * @inheritdoc
     */
    public function getCurrency()
    {
        return $this->getData(QuotationInterface::CURRENCY);
    }

    /**
     * @inheritdoc
     */
    public function setItems(array $items)
    {
        return $this->setData(QuotationInterface::ITEMS, $items);
    }

    /**
     * @inheritdoc
     */
    public function getItems()
    {
        return $this->getData(QuotationInterface::ITEMS);
    }

    /**
     * @inheritdoc
     */
    public function setShippingTo(?AddressInterface $shippingTo)
    {
        return $this->setData(QuotationInterface::SHIPPING_TO, $shippingTo);
    }

    /**
     * @inheritdoc
     */
    public function getShippingTo(): ?AddressInterface
    {
        return $this->getData(QuotationInterface::SHIPPING_TO);
    }


    public function setCoupon(string $coupon)
    {
        return $this->setData(QuotationInterface::COUPON, $coupon);
    }

    /**
     * @inheritdoc
     */
    public function getCoupon()
    {
        return $this->getData(QuotationInterface::COUPON);
    }

    /**
     * @inheritdoc
     */
    public function setEmail(string $email)
    {
        return $this->setData(QuotationInterface::EMAIL, $email);
    }

    /**
     * @inheritdoc
     */
    public function getEmail()
    {
        return $this->getData(QuotationInterface::EMAIL);
    }
}
