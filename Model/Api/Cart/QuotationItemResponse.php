<?php

namespace Simpler\Checkout\Model\Api\Cart;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\FeeInterface;
use Simpler\Checkout\Api\Data\ItemQuoteResponseInterface;
use Simpler\Checkout\Api\Data\ShippingOptionInterface;

class QuotationItemResponse extends DataObject implements ItemQuoteResponseInterface
{
    /**
     * @inheritdoc
     */
    public function setCurrency(string $currency)
    {
        return $this->setData(ItemQuoteResponseInterface::CURRENCY, $currency);
    }

    /**
     * @inheritdoc
     */
    public function getCurrency()
    {
        return $this->getData(ItemQuoteResponseInterface::CURRENCY);
    }

    /**
     * @inheritdoc
     */
    public function setItems(array $items)
    {
        return $this->setData(ItemQuoteResponseInterface::ITEMS, $items);
    }

    /**
     * @inheritdoc
     */
    public function getItems()
    {
        return $this->getData(ItemQuoteResponseInterface::ITEMS);
    }

    /**
     * @inheritdoc
     */
    public function setPaymentMethods(array $methods)
    {
        return $this->setData(ItemQuoteResponseInterface::PAYMENT_METHODS, $methods);
    }

    /**
     * @inheritdoc
     */
    public function getPaymentMethods()
    {
        return $this->getData(ItemQuoteResponseInterface::PAYMENT_METHODS);
    }

    /**
     * @inheritdoc
     */
    public function setShippingOption(?ShippingOptionInterface $shippingOption)
    {
        return $this->setData(ItemQuoteResponseInterface::SHIPPING_OPTION, $shippingOption);
    }

    /**
     * @inheritdoc
     */
    public function getShippingOption(): ?ShippingOptionInterface
    {
        return $this->getData(ItemQuoteResponseInterface::SHIPPING_OPTION);
    }

    /**
     * @inheritdoc
     */
    public function setTotalCents(int $totalCents)
    {
        return $this->setData(ItemQuoteResponseInterface::TOTAL_CENTS, $totalCents);
    }

    /**
     * @inheritdoc
     */
    public function getTotalCents()
    {
        return $this->getData(ItemQuoteResponseInterface::TOTAL_CENTS);
    }

    /**
     * @inheritdoc
     */
    public function setDiscountCents(int $discountCents)
    {
        return $this->setData(ItemQuoteResponseInterface::DISCOUNT_CENTS, $discountCents);
    }

    /**
     * @inheritdoc
     */
    public function getDiscountCents()
    {
        return $this->getData(ItemQuoteResponseInterface::DISCOUNT_CENTS);
    }

    /**
     * @inheritdoc
     */
    public function addFee(FeeInterface $fee)
    {
        $fees = $this->getData(ItemQuoteResponseInterface::FEES) ?: [];
        $fees[] = $fee;
        return $this->setData(ItemQuoteResponseInterface::FEES, $fees);
    }

    /**
     * @inheritdoc
     */
    public function setFees(array $fees)
    {
        return $this->setData(ItemQuoteResponseInterface::FEES, $fees);
    }

    /**
     * @inheritdoc
     */
    public function getFees()
    {
        return $this->getData(ItemQuoteResponseInterface::FEES);
    }
}
