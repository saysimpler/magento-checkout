<?php

namespace Simpler\Checkout\Model\Api\Cart;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\UserInterface;

class User extends DataObject implements UserInterface
{

    /**
     * @inheritDoc
     */
    public function setFirstName(string $firstName)
    {
        return $this->setData(UserInterface::FIRSTNAME, $firstName);
    }

    /**
     * @inheritDoc
     */
    public function getFirstName()
    {
        return $this->getData(UserInterface::FIRSTNAME);
    }

    /**
     * @inheritDoc
     */
    public function setLastName(string $lastName)
    {
        return $this->setData(UserInterface::LASTNAME, $lastName);
    }

    /**
     * @inheritDoc
     */
    public function getLastName()
    {
        return $this->getData(UserInterface::LASTNAME);
    }

    /**
     * @inheritDoc
     */
    public function setEmail(string $email)
    {
        return $this->setData(UserInterface::EMAIL, $email);
    }

    /**
     * @inheritDoc
     */
    public function getEmail()
    {
        return $this->getData(UserInterface::EMAIL);
    }
}
