<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\OrderInvoiceInterface;

class OrderInvoice extends DataObject implements OrderInvoiceInterface
{

    public function getCompanyName()
    {
        return $this->getData(OrderInvoiceInterface::COMPANY_NAME);
    }

    public function setCompanyName(string $name)
    {
        return $this->setData(OrderInvoiceInterface::COMPANY_NAME, $name);
    }

    public function getActivity()
    {
        return $this->getData(OrderInvoiceInterface::Activity);
    }

    public function setActivity(string $activity)
    {
        return $this->setData(OrderInvoiceInterface::Activity, $activity);
    }

    public function getTaxId()
    {
        return $this->getData(OrderInvoiceInterface::TAX_ID);
    }

    public function setTaxId(string $taxid)
    {
        return $this->setData(OrderInvoiceInterface::TAX_ID, $taxid);
    }

    public function getTaxAuthority()
    {
        return $this->getData(OrderInvoiceInterface::TAX_AUTHORITY);
    }

    public function setTaxAuthority(string $taxAuthority)
    {
        return $this->setData(OrderInvoiceInterface::TAX_AUTHORITY, $taxAuthority);
    }

    public function getCompanyAddress()
    {
        return $this->getData(OrderInvoiceInterface::COMPANY_ADDRESS);
    }

    public function setCompanyAddress(string $Address)
    {
        return $this->setData(OrderInvoiceInterface::COMPANY_ADDRESS, $Address);
    }
}
