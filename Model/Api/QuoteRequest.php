<?php

namespace Simpler\Checkout\Model\Api;

use Simpler\Checkout\Api\Data\QuotationInterface;
use Simpler\Checkout\Api\Data\QuoteRequestInterface;

class QuoteRequest extends Request implements QuoteRequestInterface
{
    /**
     * @inheritDoc
     */
    public function setQuotation(QuotationInterface $quotation)
    {
        return $this->setData(QuoteRequestInterface::QUOTATION, $quotation);
    }

    /**
     * @inheritDoc
     */
    public function getQuotation(): QuotationInterface
    {
        return $this->getData(QuoteRequestInterface::QUOTATION);
    }
}
