<?php

namespace Simpler\Checkout\Model\Api\Item;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\ItemCustomValuesInterface;

class CustomValues extends DataObject implements ItemCustomValuesInterface
{

    /**
     * @inheritDoc
     */
    public function setKey(string $key): ItemCustomValuesInterface
    {
        return $this->setData(ItemCustomValuesInterface::KEY, $key);
    }

    /**
     * @inheritDoc
     */
    public function getKey(): string
    {
        return $this->getData(ItemCustomValuesInterface::KEY);
    }

    /**
     * @inheritDoc
     */
    public function setValue(string $value): ItemCustomValuesInterface
    {
        return $this->setData(ItemCustomValuesInterface::VALUE, $value);
    }

    /**
     * @inheritDoc
     */
    public function getValue(): string
    {
        return $this->getData(ItemCustomValuesInterface::VALUE);
    }
}
