<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\PaymentMethodInterface;

class PaymentMethod extends DataObject implements PaymentMethodInterface
{

    /**
     * @inheritdoc
     */
    public function setId(string $id)
    {
        return $this->setData(PaymentMethodInterface::ID, $id);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getData(PaymentMethodInterface::ID);
    }

    /**
     * @inheritdoc
     */
    public function setType(string $type)
    {
        return $this->setData(PaymentMethodInterface::TYPE, $type);
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->getData(PaymentMethodInterface::TYPE);
    }

    /**
     * @inheritdoc
     */
    public function setName(string $name)
    {
        return $this->setData(PaymentMethodInterface::NAME, $name);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return $this->getData(PaymentMethodInterface::NAME);
    }

    /**
     * @inheritdoc
     */
    public function setNetCents(int $cents)
    {
        return $this->setData(PaymentMethodInterface::NET_CENTS, $cents);
    }

    /**
     * @inheritdoc
     */
    public function getNetCents()
    {
        return $this->getData(PaymentMethodInterface::NET_CENTS);
    }

    /**
     * @inheritdoc
     */
    public function setTaxCents(int $cents)
    {
        return $this->setData(PaymentMethodInterface::TAX_CENTS, $cents);
    }

    /**
     * @inheritdoc
     */
    public function getTaxCents()
    {
        return $this->getData(PaymentMethodInterface::TAX_CENTS);
    }

    /**
     * @inheritdoc
     */
    public function setTotalCents(int $cents)
    {
        return $this->setData(PaymentMethodInterface::TOTAL_CENTS, $cents);
    }

    /**
     * @inheritdoc
     */
    public function getTotalCents()
    {
        return $this->getData(PaymentMethodInterface::TOTAL_CENTS);
    }
}
