<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\FeeInterface;

class Fee extends DataObject implements FeeInterface
{
    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        return $this->getData(FeeInterface::TITLE);
    }

    /**
     * @inheritdoc
     */
    public function setTitle(string $title)
    {
        return $this->setData(FeeInterface::TITLE, $title);
    }

    /**
     * @inheritdoc
     */
    public function getTotalCents()
    {
        return $this->getData(FeeInterface::TOTAL_CENTS);
    }

    /**
     * @inheritdoc
     */
    public function setTotalCents(int $totalCents)
    {
        return $this->setData(FeeInterface::TOTAL_CENTS, $totalCents);
    }
}
