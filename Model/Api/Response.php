<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\ResponseInterface;

class Response extends DataObject implements ResponseInterface
{
    /**
     * @inheritDoc
     */
    public function setRequestId(string $requestId): ResponseInterface
    {
        return $this->setData(ResponseInterface::REQUEST_ID, $requestId);
    }

    /**
     * @inheritDoc
     */
    public function getRequestId()
    {
        return $this->getData(ResponseInterface::REQUEST_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStoreBaseUrl(string $storeBaseUrl)
    {
        return $this->setData(ResponseInterface::STORE_BASE_URL, $storeBaseUrl);
    }

    /**
     * @inheritDoc
     */
    public function getStoreBaseUrl()
    {
        return $this->getData(ResponseInterface::STORE_BASE_URL);
    }
}
