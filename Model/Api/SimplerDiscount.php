<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\SimplerDiscountInterface;

class SimplerDiscount extends DataObject implements SimplerDiscountInterface
{

    public function setAmount(float $amount)
    {
        return $this->setData(SimplerDiscountInterface::AMOUNT, $amount);
    }

    public function getAmount(): float
    {
        return $this->getData(SimplerDiscountInterface::AMOUNT);
    }
}
