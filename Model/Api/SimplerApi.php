<?php

namespace Simpler\Checkout\Model\Api;

use Simpler\Checkout\Api\Data\ProductsRequestInterface;
use Simpler\Checkout\Api\Data\QuoteRequestInterface;
use Simpler\Checkout\Api\Data\SubmitRequestInterface;
use Simpler\Checkout\Api\SimplerApiInterface;
use Simpler\Checkout\Controller\OrderCreateController;
use Simpler\Checkout\Controller\PickupLocationController;
use Simpler\Checkout\Controller\QuotationController;
use Simpler\Checkout\Model\Processors\Products;
use Simpler\Checkout\Model\Processors\About;

class SimplerApi implements SimplerApiInterface
{
    private $productsProcessor;
    /**
     * @var QuotationController
     */
    private $quotationController;
    private $aboutProcessor;
    /**
     * @var OrderCreateController
     */
    private $orderCreateController;
    /**
     * @var PickupLocationController
     */
    private $pickupLocationController;

    public function __construct(
        Products                 $productsProcessor,
        OrderCreateController    $orderCreateController,
        QuotationController      $quotationController,
        About                    $aboutProcessor,
        PickupLocationController $pickupLocationController
    ) {
        $this->productsProcessor = $productsProcessor;
        $this->quotationController = $quotationController;
        $this->aboutProcessor = $aboutProcessor;
        $this->orderCreateController = $orderCreateController;
        $this->pickupLocationController = $pickupLocationController;
    }

    /**
     * @inheritDoc
     */
    public function quote(QuoteRequestInterface $cart)
    {
        return $this->quotationController->handle($cart);
    }

    /**
     * @inheritDoc
     */
    public function products(ProductsRequestInterface $products)
    {
        return $this->productsProcessor->processRequest($products);
    }

    /**
     * @inheritDoc
     */
    public function submit(SubmitRequestInterface $order)
    {
        return $this->orderCreateController->handle($order);
    }

    /**
     * @inheritDoc
     */
    public function about()
    {
        return $this->aboutProcessor->processRequest();
    }

    /**
     * @inheritDoc
     */
    public function pickupLocations()
    {
        return $this->pickupLocationController->index();
    }
}
