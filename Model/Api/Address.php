<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\AddressInterface;

class Address extends DataObject implements AddressInterface
{
    /**
     * @inheritDoc
     */
    public function setFirstname(string $firstname)
    {
        return $this->setData(AddressInterface::FIRSTNAME, $firstname);
    }

    /**
     * @inheritDoc
     */
    public function setLastname(string $lastname)
    {
        return $this->setData(AddressInterface::LASTNAME, $lastname);
    }

    /**
     * @inheritDoc
     */
    public function setTelephone(string $phone)
    {
        return $this->setData(AddressInterface::TELEPHONE, $phone);
    }

    /**
     * @inheritDoc
     */
    public function setStreet(string $street)
    {
        return $this->setData(AddressInterface::STREET, $street);
    }

    /**
     * @inheritDoc
     */
    public function setCity($city)
    {
        return $this->setData(AddressInterface::CITY, $city);
    }

    /**
     * @inheritDoc
     */
    public function setCountryId($country)
    {
        return $this->setData(AddressInterface::COUNTRY_ID, $country);
    }

    /**
     * @inheritDoc
     */
    public function setPostcode($postcode)
    {
        return $this->setData(AddressInterface::POSTCODE, $postcode);
    }

    /**
     * @inheritDoc
     */
    public function setNotes($notes)
    {
        return $this->setData(AddressInterface::NOTES, $notes);
    }

    /**
     * @inheritDoc
     */
    public function setRegion($region)
    {
        return $this->setData(AddressInterface::REGION, $region);
    }

    /**
     * @inheritDoc
     */
    public function setPickupStoreId($storeId)
    {
        return $this->setData(AddressInterface::PICKUP_STORE_ID, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getFirstname()
    {

        return $this->getData(AddressInterface::FIRSTNAME);
    }

    /**
     * @inheritDoc
     */
    public function getLastname()
    {
        return $this->getData(AddressInterface::LASTNAME);
    }

    /**
     * @inheritDoc
     */
    public function getTelephone()
    {
        return $this->getData(AddressInterface::TELEPHONE);
    }

    /**
     * @inheritDoc
     */
    public function getStreet()
    {
        return $this->getData(AddressInterface::STREET);
    }

    /**
     * @inheritDoc
     */
    public function getCity()
    {
        return $this->getData(AddressInterface::CITY);
    }

    /**
     * @inheritDoc
     */
    public function getPostcode()
    {
        return $this->getData(AddressInterface::POSTCODE);
    }

    /**
     * @inheritDoc
     */
    public function getCountryId()
    {
        return $this->getData(AddressInterface::COUNTRY_ID);
    }

    /**
     * @inheritDoc
     */
    public function getNotes()
    {
        return $this->getData(AddressInterface::NOTES);
    }

    /**
     * @inheritDoc
     */
    public function getRegion()
    {
        return $this->getData(AddressInterface::REGION);
    }

    /**
     * @inheritDoc
     */
    public function getPickupStoreId()
    {
        return $this->getData(AddressInterface::PICKUP_STORE_ID);
    }

    /**
     * @inheritDoc
     */
    public function toMagentoQuoteAddressArray(): array
    {
        foreach (AddressInterface::ATTRIBUTES as $attribute) {
            $addressData[$attribute] = $this->getData($attribute);
        }
        if (empty((string)$addressData['telephone'])) {
            $addressData['telephone'] = '00000000';
            $this->setData('telephone', '00000000');
        }
        if (is_null($addressData['firstname'])) {
            $addressData['firstname'] = 'simpler';
            $this->setData('firstname', 'simpler');
        }

        if (is_null($addressData['lastname'])) {
            $addressData['lastname'] = 'simpler';
            $this->setData('lastname', 'simpler');
        }


        $addressData['save_in_address_book'] = 1;

        return $addressData;
    }

    /**
     * @inheritdoc
     */
    public function isEmpty()
    {
        return count($this->_data) == 0;
    }
}
