<?php

namespace Simpler\Checkout\Model\Api\About;

use Simpler\Checkout\Api\About\MagentoInfoInterface;
use Magento\Framework\DataObject;

class MagentoInfo extends DataObject implements MagentoInfoInterface
{

    /**
     * @inheritDoc
     */
    public function setVersion(string $version)
    {
        return $this->setData(MagentoInfoInterface::VERSION, $version);
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return $this->getData(MagentoInfoInterface::VERSION);
    }

      /**
     * @inheritDoc
     */
    public function setModules($modules)
    {
        return $this->setData(MagentoInfoInterface::MODULES, $modules);
    }

    /**
     * @inheritDoc
     */
    public function getModules()
    {
        return $this->getData(MagentoInfoInterface::MODULES);
    }

}
