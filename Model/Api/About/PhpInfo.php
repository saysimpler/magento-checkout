<?php

namespace Simpler\Checkout\Model\Api\About;

use Simpler\Checkout\Api\About\PhpInfoInterface;
use Magento\Framework\DataObject;

class PhpInfo extends DataObject implements PhpInfoInterface
{

   /**
     * @inheritDoc
     */
    public function setVersion(string $version)
    {
        return $this->setData(PhpInfoInterface::VERSION, $version);
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return $this->getData(PhpInfoInterface::VERSION);
    }

      /**
     * @inheritDoc
     */
    public function setExtensions($extensions)
    {
        return $this->setData(PhpInfoInterface::EXTENSIONS, $extensions);
    }

    /**
     * @inheritDoc
     */
    public function getExtensions()
    {
        return $this->getData(PhpInfoInterface::EXTENSIONS);
    }

}
