<?php

namespace Simpler\Checkout\Model\Api\About;

use Simpler\Checkout\Api\About\AboutInterface;
use Magento\Framework\DataObject;

class About extends DataObject implements AboutInterface
{

    /**
     * @inheritDoc
     */
    public function setDbVersion($dbversion)
    {
        return $this->setData(AboutInterface::Db_Version, $dbversion);
    }

    /**
     * @inheritDoc
     */
    public function getDbVersion()
    {
        return $this->getData(AboutInterface::Db_Version);
    }

      /**
     * @inheritDoc
     */
    public function setPHPInfo($phpInfo)
    {
        return $this->setData(AboutInterface::PHPInfo, $phpInfo);
    }

    /**
     * @inheritDoc
     */
    public function getPHPInfo()
    {
        return $this->getData(AboutInterface::PHPInfo);
    }

      /**
     * @inheritDoc
     */
    public function setMagento($magentoInfo)
    {
        return $this->setData(AboutInterface::Magento, $magentoInfo);
    }

    /**
     * @inheritDoc
     */
    public function getMagento()
    {
        return $this->getData(AboutInterface::Magento);
    }

}
