<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\PickupLocationInterface;
use Simpler\Checkout\Api\Data\PickupLocationsInterface;

class PickupLocations extends DataObject implements PickupLocationsInterface
{

    /** @inheritdoc */
    public function getPickupLocations()
    {
        return $this->getData(PickupLocationsInterface::PICKUP_LOCATIONS) ?: [];
    }

    /** @inheritdoc */
    public function setPickupLocations(array $locations)
    {
        return $this->setData(PickupLocationsInterface::PICKUP_LOCATIONS, $locations);
    }

    /** @inheritdoc */
    public function addPickupLocation(PickupLocationInterface $location)
    {
        $locations = $this->getPickupLocations();
        $locations[] = $location;
        $this->setPickupLocations($locations);

        return $this;
    }
}
