<?php

namespace Simpler\Checkout\Model\Api;

use Simpler\Checkout\Api\Data\QuoteResponseInterface;

class QuoteResponse extends Response implements QuoteResponseInterface
{
    /**
     * @inheritDoc
     */
    public function getQuotations()
    {
        return $this->getData(QuoteResponseInterface::QUOTATIONS);
    }

    /**
     * @inheritDoc
     */
    public function setQuotations(array $quotations)
    {
        return $this->setData(QuoteResponseInterface::QUOTATIONS, $quotations);
    }
}
