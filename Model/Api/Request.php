<?php

namespace Simpler\Checkout\Model\Api;

use Magento\Framework\DataObject;
use Simpler\Checkout\Api\Data\RequestInterface;

class Request extends DataObject implements RequestInterface
{
    /**
     * @inheritDoc
     */
    public function setRequestId(string $requestId): RequestInterface
    {
        return $this->setData(RequestInterface::REQUEST_ID, $requestId);
    }

    /**
     * @inheritDoc
     */
    public function getRequestId()
    {
        return $this->getData(RequestInterface::REQUEST_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStoreId(int $storeId): RequestInterface
    {
        return $this->setData(RequestInterface::STORE_ID, $storeId);
    }

     /**
     * @inheritDoc
     */
    public function getStoreId()
    {
        return $this->getData(RequestInterface::STORE_ID);
    }

     /**
     * @inheritDoc
     */
    public function setWebsiteId(int $websiteId): RequestInterface
    {
        return $this->setData(RequestInterface::WEBSITE_ID, $websiteId);
    }

     /**
     * @inheritDoc
     */
    public function getWebsiteId()
    {
        return $this->getData(RequestInterface::WEBSITE_ID);
    }
}
