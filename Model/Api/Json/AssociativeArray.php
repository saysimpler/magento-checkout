<?php

namespace Simpler\Checkout\Model\Api\Json;

use Simpler\Checkout\Api\Json\AssociativeArrayItemInterface;
use Magento\Framework\DataObject;

class AssociativeArray extends DataObject implements AssociativeArrayItemInterface
{

     /**
     * @inheritDoc
     */
    public function setKey(string $key): AssociativeArrayItemInterface
    {
        return $this->setData(AssociativeArrayItemInterface::KEY, $key);
    }

     /**
     * @inheritDoc
     */
    public function getKey(): string
    {
        return $this->getData(AssociativeArrayItemInterface::KEY);
    }

     /**
     * @inheritDoc
     */
    public function setValue(string $value): AssociativeArrayItemInterface
    {
        return $this->setData(AssociativeArrayItemInterface::VALUE, $value);
    }

     /**
     * @inheritDoc
     */
    public function getValue(): string
    {
        return $this->getData(AssociativeArrayItemInterface::VALUE);
    }
}