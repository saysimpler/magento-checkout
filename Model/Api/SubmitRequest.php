<?php

namespace Simpler\Checkout\Model\Api;

use Simpler\Checkout\Api\Data\AddressInterface;
use Simpler\Checkout\Api\Data\OrderBoxNowInterface;
use Simpler\Checkout\Api\Data\OrderInvoiceInterface;
use Simpler\Checkout\Api\Data\SimplerDiscountInterface;
use Simpler\Checkout\Api\Data\SubmitRequestInterface;

class SubmitRequest extends Request implements SubmitRequestInterface
{
    public function setNote(string $note)
    {
        return $this->setData(SubmitRequestInterface::NOTE, $note);
    }

    public function getNote()
    {
        return $this->getData(SubmitRequestInterface::NOTE);
    }

    public function setUser(\Simpler\Checkout\Api\Data\UserInterface $user)
    {
        return $this->setData(SubmitRequestInterface::USER, $user);
    }

    public function getUser()
    {
        return $this->getData(SubmitRequestInterface::USER);
    }

    public function setItems(array $items)
    {
        return $this->setData(SubmitRequestInterface::ITEMS, $items);
    }

    public function getItems()
    {
        return $this->getData(SubmitRequestInterface::ITEMS);
    }

    public function setCurrency(string $currency)
    {
        return $this->setData(SubmitRequestInterface::CURRENCY, $currency);

    }

    public function getCurrency()
    {
        return $this->getData(SubmitRequestInterface::CURRENCY);
    }

    public function setShippingTo(AddressInterface $sippingTo)
    {
        return $this->setData(SubmitRequestInterface::SHIPPING_TO, $sippingTo);

    }

    public function getShippingTo()
    {
        return $this->getData(SubmitRequestInterface::SHIPPING_TO);
    }

    public function setCoupon(string $coupon)
    {
        return $this->setData(SubmitRequestInterface::COUPON, $coupon);
    }

    public function getCoupon()
    {
        return $this->getData(SubmitRequestInterface::COUPON);
    }

    public function setShippingMethod(string $shippingMethod)
    {
        return $this->setData(SubmitRequestInterface::SHIPPING_METHOD, $shippingMethod);
    }

    public function getShippingMethod()
    {
        return $this->getData(SubmitRequestInterface::SHIPPING_METHOD);
    }

    public function setPaymentMethod(string $paymentMethod)
    {
        return $this->setData(SubmitRequestInterface::PAYMENT_METHOD, $paymentMethod);
    }

    public function getPaymentMethod()
    {
        return $this->getData(SubmitRequestInterface::PAYMENT_METHOD);
    }

    public function getSimplerDiscount()
    {
        return $this->getData(SubmitRequestInterface::SIMPLER_DISCOUNT);
    }

    public function setSimplerDiscount(SimplerDiscountInterface $simplerDiscount)
    {
        return $this->setData(SubmitRequestInterface::SIMPLER_DISCOUNT, $simplerDiscount);

    }

    public function setSimplerOrderId(string $simplerOrderId)
    {
        return $this->setData(SubmitRequestInterface::SIMPLER_ORDER_ID, $simplerOrderId);
    }

    public function getSimplerOrderId()
    {
        return $this->getData(SubmitRequestInterface::SIMPLER_ORDER_ID);
    }

    public function setInvoice(OrderInvoiceInterface $invoice)
    {
        return $this->setData(SubmitRequestInterface::INVOICE, $invoice);
    }

    public function getInvoice()
    {
        return $this->getData(SubmitRequestInterface::INVOICE);
    }

    public function setBoxNow(OrderBoxNowInterface $boxNow)
    {
        return $this->setData(SubmitRequestInterface::BOXNOW, $boxNow);
    }

    public function getBoxNow()
    {
        return $this->getData(SubmitRequestInterface::BOXNOW);
    }
}



