<?php

namespace Simpler\Checkout\Model\Processors;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Checkout\Api\PaymentInformationManagementInterface;
use Magento\Directory\Model\AllowedCountries;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\OfflinePayments\Model\Cashondelivery;
use Magento\Quote\Api\{CartRepositoryInterface, ShipmentEstimationInterface};
use Magento\Quote\Api\Data\{AddressInterfaceFactory, ShippingMethodInterface};
use Magento\Store\Model\App\Emulation;
use Simpler\Checkout\Api\Data\{
    ItemQuoteResponseInterfaceFactory,
    PaymentMethodInterface,
    PaymentMethodInterfaceFactory,
    QuotationInterface,
    QuoteRequestInterface,
    ShippingOptionInterface,
    ShippingOptionInterfaceFactory
};
use Simpler\Checkout\Exception\{
    InapplicableCouponException,
    InvalidCouponException,
    InvalidProductException,
    OutOfStockProductException,
    UnavailableProductException,
    UnshippableLocationException
};
use Simpler\Checkout\Event\Quote\AfterCustomerAssignedEvent;
use Simpler\Checkout\Event\Quote\AfterPaymentMethodsCollectedEvent;
use Simpler\Checkout\Event\Quote\AfterProductsAddedEvent;
use Simpler\Checkout\Event\Quote\AfterQuoteCreatedEvent;
use Simpler\Checkout\Event\Quote\AfterDiscountsCollectedEvent;
use Simpler\Checkout\Event\Quote\AfterShippingMethodsEstimatedEvent;
use Simpler\Checkout\Event\Quote\AfterTotalsCollectedEvent;
use Simpler\Checkout\Event\Quote\BeforeCustomerAssignedEvent;
use Simpler\Checkout\Event\Quote\BeforeProductsAddedEvent;
use Simpler\Checkout\Event\Quote\BeforeDiscountsCollectedEvent;
use Simpler\Checkout\Event\Quote\BeforeTotalsCollectedEvent;
use Simpler\Checkout\Event\Event;
use Simpler\Checkout\Helper\Money;
use Simpler\Checkout\Model\Api\Cart\{Quotation, QuotationItemResponse};


class Quote
{
    /**
     * @var Cart
     */
    private $cartProcessor;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var ShippingOptionInterfaceFactory
     */
    private $shippingOptionFactory;
    /**
     * @var ShipmentEstimationInterface
     */
    private $shipmentEstimation;
    /**
     * @var ItemQuoteResponseInterfaceFactory
     */
    private $quotationResponseFactory;
    /**
     * @var AddressInterfaceFactory
     */
    private $addressFactory;
    /**
     * @var PaymentMethodInterfaceFactory
     */
    private $paymentMethodFactory;
    /**
     * @var DiscountCollectorInterface
     */
    private $discountCollector;
    /**
     * @var PaymentInformationManagementInterface
     */
    private $paymentInformation;
    /**
     * @var AllowedCountries
     */
    private $allowedCountries;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var \Magento\Quote\Model\Quote
     */
    private $quote;

    public function __construct(
        Cart                              $cartProcessor,
        CartRepositoryInterface           $cartRepository,
        CustomerRepositoryInterface       $customerRepository,
        AddressInterfaceFactory           $addressFactory,
        ShippingOptionInterfaceFactory    $shippingOptionFactory,
        ShipmentEstimationInterface       $shipmentEstimation,
        ItemQuoteResponseInterfaceFactory $quotationResponseFactory,
        PaymentMethodInterfaceFactory     $paymentMethodFactory,
        DiscountCollectorInterface        $discountCollector,
        PaymentInformationManagementInterface $paymentInformation,
        AllowedCountries                  $allowedCountries,
        Emulation                         $emulation,
        ManagerInterface                  $eventManager
    ) {
        $this->cartProcessor = $cartProcessor;
        $this->cartRepository = $cartRepository;
        $this->customerRepository = $customerRepository;
        $this->shippingOptionFactory = $shippingOptionFactory;
        $this->shipmentEstimation = $shipmentEstimation;
        $this->quotationResponseFactory = $quotationResponseFactory;
        $this->paymentMethodFactory = $paymentMethodFactory;
        $this->addressFactory = $addressFactory;
        $this->discountCollector = $discountCollector;
        $this->paymentInformation = $paymentInformation;
        $this->allowedCountries = $allowedCountries;
        $this->emulation = $emulation;
        $this->eventManager = $eventManager;
    }

    /**
     * @param  QuoteRequestInterface  $request
     * @return Quotation[]
     * @throws CouldNotSaveException
     * @throws InapplicableCouponException
     * @throws InvalidCouponException
     * @throws InvalidProductException
     * @throws NoSuchEntityException
     * @throws OutOfStockProductException
     * @throws UnavailableProductException
     * @throws UnshippableLocationException
     */
    public function quote(QuoteRequestInterface $request): array
    {
        try {
            $this->emulation->startEnvironmentEmulation($request->getStoreId());

            $this->quote = $this->cartProcessor->createQuote();
            $this->dispatch(new AfterQuoteCreatedEvent($this->quote));

            $this->assignCustomer($request);

            $this->dispatch(new BeforeProductsAddedEvent($this->quote, $items = $request->getQuotation()->getItems()));
            $this->cartProcessor->addProducts($items, $this->quote);
            $this->dispatch(new AfterProductsAddedEvent($this->quote, $items));

            $this->quote->setQuoteCurrencyCode($request->getQuotation()->getCurrency());

            $this->cartProcessor->maybeApplyCouponCode($request->getQuotation()->getCoupon(), $this->quote);

            $this->dispatch(new BeforeTotalsCollectedEvent($this->quote));
            $this->quote = $this->cartProcessor->updateQuoteTotals($this->quote);
            $this->dispatch(new AfterTotalsCollectedEvent($this->quote));

            return $this->createQuotations($request->getQuotation());
        } finally {
            $this->emulation->stopEnvironmentEmulation();
            $this->cartRepository->delete($this->quote);
        }
    }

    /**
     * @return Quotation[]
     * @throws UnshippableLocationException
     */
    private function createQuotations(QuotationInterface $cart): array
    {
        $quotations = [];

        /** @var QuotationItemResponse $quotation */
        $quotation = $this->quotationResponseFactory->create()
            ->addData($cart->getData())
            ->setPaymentMethods([])
            ->setTotalCents(Money::toCents($this->quote->getGrandTotal()));

        $shippingMethods = $this->getShippingMethods();
        $this->dispatch(new AfterShippingMethodsEstimatedEvent($this->quote, $shippingMethods));

        // collect discounts
        $this->cartRepository->save($this->quote);

        if ($cart->getShippingTo() === null || $cart->getShippingTo()->isEmpty()) {
            $this->dispatch(new BeforeDiscountsCollectedEvent($this->quote, $quotation));
            $this->discountCollector->collect($this->quote, $quotation);
            $this->dispatch(new AfterDiscountsCollectedEvent($this->quote, $quotation));
            return [$quotation];
        }

        if (!isset($this->allowedCountries->getAllowedCountries()[$cart->getShippingTo()->getCountryId()])) {
            throw new UnshippableLocationException("country is not allowed");
        }
        if (empty($shippingMethods)) {
            throw new UnshippableLocationException("could not determine any shipping methods for quote");
        }

        foreach ($shippingMethods as $shippingMethod) {
            if (!$shippingMethod->getAvailable()) {
                continue;
            }

            $this->dispatch(new BeforeDiscountsCollectedEvent($this->quote, $quotation));
            $this->discountCollector->collect($this->quote, $quotation);
            $this->dispatch(new AfterDiscountsCollectedEvent($this->quote, $quotation));

            $quotationWithShipping = $this->quotationResponseFactory->create()->addData($quotation->getData());
            $shippingTotalCostCents = Money::toCents($shippingMethod->getAmount());
            $quotationWithShipping->setTotalCents($quotationWithShipping->getTotalCents() + $shippingTotalCostCents);

            $quotationWithShipping->setShippingOption($this->createShippingOption(
                $shippingMethod,
                $shippingTotalCostCents
            ));

            $this->quote->getShippingAddress()->setShippingMethod(Cart::getShippingMethodId($shippingMethod));
            $this->cartRepository->save($this->quote);

            $paymentMethods = $this->getPaymentMethods();
            $this->dispatch(new AfterPaymentMethodsCollectedEvent($this->quote, $paymentMethods));
            $quotationWithShipping->setPaymentMethods($paymentMethods);

            $quotations[] = $quotationWithShipping;
        }

        return $quotations;
    }

    /**
     * @param  ShippingMethodInterface  $shippingMethod
     * @param  int                      $total
     * @return ShippingOptionInterface
     */
    private function createShippingOption(ShippingMethodInterface $shippingMethod, int $total): ShippingOptionInterface
    {
        $shippingOption = $this->shippingOptionFactory->create();
        $shippingOption->setId(Cart::getShippingMethodId($shippingMethod))
            ->setName(Cart::getShippingMethodName($shippingMethod))
            ->setTotalCents($total)
            ->setCostCents(Money::toCents($shippingMethod->getPriceExclTax()))
            ->setTaxCents(Money::toCents($shippingMethod->getPriceInclTax() - $shippingMethod->getPriceExclTax()))
            ->setType("DELIVERY");

        $this->eventManager->dispatch('simpler_checkout_around_create_shipping_option', [
            'shipping_method' => $shippingMethod,
            'shipping_option' => $shippingOption
        ]);
        return $shippingOption;
    }

    /**
     * Get configured shipping methods regardless of availability.
     *
     * @return ShippingMethodInterface[]
     */
    private function getShippingMethods(): array
    {
        return $this->shipmentEstimation->estimateByExtendedAddress(
            $this->quote->getId(),
            $this->quote->getShippingAddress()
        );
    }

    /**
     * Get configured & supported payment methods
     *
     * @return PaymentMethodInterface[]
     */
    private function getPaymentMethods(): array
    {
        $ret = [];
        $paymentMethods = $this->paymentInformation->getPaymentInformation($this->quote->getId());
        foreach ($paymentMethods->getPaymentMethods() as $pm) {
            if ($pm->getCode() == Cashondelivery::PAYMENT_METHOD_CASHONDELIVERY_CODE) {
                $ret[] = $this->paymentMethodFactory->create()
                    ->setId($pm->getCode())
                    ->setType('COD')
                    ->setName($pm->getTitle())
                    ->setNetCents(0)
                    ->setTaxCents(0)
                    ->setTotalCents(0);
            }
        }
        return $ret;
    }

    /**
     * @param  QuoteRequestInterface  $request
     *
     * @return void
     * @throws UnshippableLocationException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function assignCustomer(QuoteRequestInterface $request): void
    {
        $shipTo = $request->getQuotation()->getShippingTo();
        if (is_null($shipTo) || $shipTo->isEmpty()) {
            $shippingAddress = $billingAddress = $this->addressFactory->create();
        } else {
            $billingAddress = $this->cartProcessor->addressToMagentoBillingAddress($shipTo);
            $shippingAddress = $this->cartProcessor->addressToMagentoShippingAddress($shipTo);
        }

        $customer = null;
        if ($email = $request->getQuotation()->getEmail()) {
            try {
                $customer = $this->customerRepository->get($email);
            } catch (NoSuchEntityException $e) {
            }
        }

        $this->dispatch(
            new BeforeCustomerAssignedEvent(
                $this->quote,
                $customer,
                $shippingAddress,
                $billingAddress
            )
        );

        if ($customer) {
            $this->quote->assignCustomerWithAddressChange(
                $customer,
                $billingAddress,
                $shippingAddress
            );

            if ($shipTo !== null && !$shipTo->isEmpty()) {
                if (is_array($errors = $this->quote->getShippingAddress()->validate())) {
                    throw new UnshippableLocationException(json_encode($errors));
                }

                if (is_array($errors = $this->quote->getBillingAddress()->validate())) {
                    throw new UnshippableLocationException(json_encode($errors));
                }
            }
        } else {
            $this->quote->setCustomerIsGuest(true);
            $this->quote->setShippingAddress($shippingAddress);
            $this->quote->setBillingAddress($billingAddress);
        }

        $this->dispatch(
            new AfterCustomerAssignedEvent(
                $this->quote,
                $customer,
                $shippingAddress,
                $billingAddress
            )
        );
    }

    private function dispatch(Event $event)
    {
        $this->eventManager->dispatch(
            $event->getName(),
            $event->toDispatchable()
        );
    }
}
