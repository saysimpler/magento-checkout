<?php

namespace Simpler\Checkout\Model\Processors;

use Magento\Quote\Model\Quote;
use Simpler\Checkout\Api\Data\ItemQuoteResponseInterface;

interface DiscountCollectorInterface
{
    /**
     * @param Magento\Quote\Model\Quote $magentoQuote
     * @param ItemQuoteResponseInterfac $simplerQuote
     */
    public function collect(Quote $magentoQuote, ItemQuoteResponseInterface $simplerQuote);
}
