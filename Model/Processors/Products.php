<?php

namespace Simpler\Checkout\Model\Processors;

use Magento\Bundle\Model\Product\Type as BundleType;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManager;
use Simpler\Checkout\Api\Data\ItemResponseInterfaceFactory;
use Simpler\Checkout\Api\Data\ProductsRequestInterface;
use Simpler\Checkout\Api\Data\ProductsResponseInterface;
use Simpler\Checkout\Api\Data\ProductsResponseInterfaceFactory;
use Simpler\Checkout\Api\Data\ItemResponseInterface;
use Simpler\Checkout\Api\Data\ConfigurableOptionInterfaceFactory;
use Simpler\Checkout\Event\Event;
use Simpler\Checkout\Event\Products\{AfterProductConfigurationsCollectedEvent, AfterProductsAddedEvent, AfterConfigurableChildrenCollectedEvent};
use Simpler\Checkout\Helper\StockHelper;

class Products
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductResponseInterfaceFactory
     */
    private $productsResponseFactory;
    /**
     * @var ItemResponseInterfaceFactory
     */
    private $itemResponseFactory;
    /**
     * @var Configurable
     */
    private $configurableType;
    /**
     * @var Grouped
     */
    private $groupedType;
    /**
     * @var BundleType
     */
    private $bundleType;
    /**
     * @var ConfigurableOptionInterfaceFactory
     */
    private $configOptionFactory;
    /**
     * @var configurableOptions|mixed
     */
    private $configurableOptions;
    /**
     * @var childproducts|mixed
     */
    private $childproducts;
    /**
     * @var StockHelper $stockHelper
     */
    private $stockHelper;
    /**
     * @var StoreManager
     */
    private $storeManager;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var ManagerInterface
     */
    private $eventManager;

    public function __construct(
        ProductRepositoryInterface           $productRepository,
        ProductsResponseInterfaceFactory     $productsResponseFactory,
        ItemResponseInterfaceFactory         $itemResponseInterfaceFactory,
        Configurable                         $configurableType,
        Grouped                              $groupedType,
        BundleType                           $bundleType,
        ConfigurableOptionInterfaceFactory   $configOptionFactory,
        StockHelper                          $stockHelper,
        StoreManager                         $storeManager,
        Emulation                            $emulation,
        ManagerInterface                     $eventManager
    ) {
        $this->productRepository = $productRepository;
        $this->productsResponseFactory = $productsResponseFactory;
        $this->itemResponseFactory = $itemResponseInterfaceFactory;
        $this->configurableType = $configurableType;
        $this->groupedType = $groupedType;
        $this->bundleType = $bundleType;
        $this->configOptionFactory = $configOptionFactory;
        $this->storeManager = $storeManager;
        $this->stockHelper = $stockHelper;
        $this->emulation = $emulation;
        $this->eventManager = $eventManager;
    }

    /**
     * @param ProductsRequestInterface $products
     * @return ProductsResponseInterface
     * @throws NoSuchEntityException
     */
    public function processRequest(ProductsRequestInterface $request)
    {
        $this->emulation->startEnvironmentEmulation($request->getStoreId());
        $response = $this->productsResponseFactory->create();
        $response->setRequestId($request->getRequestId())
            ->setStoreBaseUrl($this->storeManager->getStore()->getBaseUrl());

        foreach ($request->getItems() as $item) {
            $product = $this->productRepository->get($item->getId());
            $response->addItem($this->buildItemResponse($product));
        }
        $this->dispatch(new AfterProductsAddedEvent($response));

        $this->emulation->stopEnvironmentEmulation();
        return $response;
    }

    private function getImage($product)
    {
        $url = $product->getMediaGalleryImages()->getFirstItem()->getData("url");
        if (!$url) {
            $parentId = $this->getParentId($product);
            if (!$parentId) {
                $parentIds = $this->groupedType->getParentIdsByChild($product->getId());
                $parentId = array_shift($parentIds);
            }
            if ($parentId) {
                $parentProduct = $this->productRepository->getById($parentId);
                $url = $parentProduct->getMediaGalleryImages()->getFirstItem()->getData("url");
            }
        }
        return $url;
    }

    private function getConfigurableOptions(ProductInterface $childProduct)
    {
        $parentId = $this->getParentId($childProduct);
        $parentProduct = $this->productRepository->getById($parentId);
        $options = $this->configurableType->getConfigurableOptions($parentProduct);

        $itemOptions = [];
        foreach ($options as $optionsSetId => $optionsSet) {
            foreach ($optionsSet as $childOptionsId => $childOptions) {
                if ($childOptions["sku"] == $childProduct->getSku()) {
                    $option = $this->configOptionFactory->create();
                    $option->setConfigurableOptionId($optionsSetId);
                    $option->setConfigurableOptionLabel(
                        array_key_exists('super_attribute_label', $childOptions) ? $childOptions['super_attribute_label'] : (array_key_exists('attribute_code', $childOptions) ? $childOptions['attribute_code'] : $childOptions['default_title'])
                    );
                    $option->setConfigurableOptionValueId($childOptions['value_index']);
                    $option->setConfigurableOptionValueLabel(array_key_exists('option_title', $childOptions) ? $childOptions['option_title'] : $childOptions['default_title']);
                    $this->dispatch(new AfterProductConfigurationsCollectedEvent($option, $childProduct, $optionsSet));
                    array_push($itemOptions, $option);
                }
            }
        }
        return $itemOptions;
    }

    private function getChildProducts(ProductInterface $parentProduct)
    {
        $childItemResponses = [];
        $event = new AfterConfigurableChildrenCollectedEvent(
            $parentProduct,
            $this->configurableType->getChildrenIds($parentProduct->getId())
        );
        $this->dispatch($event);
        $childrenIds = $event->getChildrenIds();
        foreach ($childrenIds[0] as $childId) {
            $childProduct = $this->productRepository->getById($childId);
            $itemResponse = $this->buildItemResponse($childProduct);
            array_push($childItemResponses, $itemResponse);
        }
        return $childItemResponses;
    }


    private function buildItemResponse(ProductInterface $product): ItemResponseInterface
    {
        $itemResponse = $this->itemResponseFactory->create();
        $productType = $product->getTypeId();
        $imageUrl = $this->getImage($product);
        $shippable = in_array($product->getTypeId(), ["simple", "grouped", "bundle", "configurable"]);

        // gather configurations if requested product is a configuration
        $parentId = $this->getParentId($product);
        if ($parentId) {
            if ($this->productRepository->getById($parentId)->getTypeId() == "configurable") {
                $this->configurableOptions = $this->getConfigurableOptions($product);
                $itemResponse->setConfigurations($this->configurableOptions);
            }
        }

        $stock = 0;
        if ($productType == "configurable") {
            $this->childproducts = $this->getChildProducts($product); //recurse
            $itemResponse->setChildren($this->childproducts);
        } else {
            $stock = $this->stockHelper->getStockQuantity($product);
        }

        $itemResponse
            ->setId($product->getId())
            ->setTitle($product->getName())
            ->setPrice(0)
            ->setTotalCents((int)($product->getFinalPrice() * 100))
            ->setTaxCents((int)0)
            ->setSku($product->getData('sku'))
            ->setShippable($shippable)
            ->setImageUrl((string)$imageUrl)
            ->setType($productType)
            ->setQuantity($stock);

        return $itemResponse;
    }

    private function getParentId(ProductInterface $product)
    {
        $parentIds = $this->configurableType->getParentIdsByChild($product->getId());
        return array_shift($parentIds);
    }

    private function dispatch(Event $event)
    {
        $this->eventManager->dispatch(
            $event->getName(),
            $event->toDispatchable()
        );
    }
}
