<?php

namespace Simpler\Checkout\Model\Processors;

use Simpler\Checkout\Api\About\AboutInterfaceFactory;
use Simpler\Checkout\Api\About\AboutInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Module\FullModuleList;
use Magento\Framework\Module\Manager;
use Simpler\Checkout\Api\About\MagentoInfoInterfaceFactory;
use Simpler\Checkout\Api\About\PhpInfoInterfaceFactory;

class About
{

  private $resource;
  private $aboutFactory;
  private $productMetadata;
  private $fullModuleList;
  private $moduleManager;
  private $magentoInfo;
  private $phpInfo;

  public function __construct(
    ResourceConnection       $resource,
    AboutInterfaceFactory $aboutFactory,
    ProductMetadataInterface  $productMetadata,
    FullModuleList $fullModuleList,
    Manager $moduleManager,
    MagentoInfoInterfaceFactory $magentoInfo,
    PhpInfoInterfaceFactory $phpInfo
  ) {
    $this->resource = $resource;
    $this->aboutFactory = $aboutFactory;
    $this->productMetadata = $productMetadata;
    $this->fullModuleList = $fullModuleList;
    $this->moduleManager = $moduleManager;
    $this->phpInfo = $phpInfo;
    $this->magentoInfo = $magentoInfo;
  }

  /**
   *
   * @return AboutInterface
   */
  public function processRequest()
  {
    $about = $this->aboutFactory->create();

    $connection = $this->resource->getConnection();
    $dbVersion = $this->getDBVersion($connection);

    $about
      ->setDbVersion($dbVersion)
      ->setPHPInfo($this->getPHPInfo())
      ->setMagento($this->getMagentoInfo());

    return $about;
  }

  private function getDBVersion(AdapterInterface $connection): string
  {
    $query = "select version();";

    $dbVersion = $connection->fetchOne($query);

    return $dbVersion;
  }

  private function getMagentoInfo()
  {

    $modules = [];
    foreach ($this->fullModuleList->getAll() as $name => $info) {
      $modules[] = [
        'name' => $info['name'],
        'version' => $info['setup_version'],
        'active' => $this->moduleManager->isEnabled($info['name']),
      ];
    }

    $magentoInfo = $this->magentoInfo->create()
      ->setVersion($this->productMetadata->getVersion())
      ->setModules($modules);

    return $magentoInfo;
  }

  private function getPHPInfo()
  {
    $phpInfo = $this->phpInfo->create()
      ->setVersion(phpversion())
      ->setExtensions(get_loaded_extensions());

    return $phpInfo;
  }
}
