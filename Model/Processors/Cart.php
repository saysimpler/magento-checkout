<?php

namespace Simpler\Checkout\Model\Processors;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Product as ProductHelper;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Model\Address\AbstractAddress;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\{CouldNotSaveException, LocalizedException, NoSuchEntityException};
use Magento\Quote\Api\{CartRepositoryInterface, CouponManagementInterface, GuestCartManagementInterface};
use Magento\Quote\Api\Data\{AddressInterfaceFactory, ShippingMethodInterface};
use Magento\Quote\Model\MaskedQuoteIdToQuoteId;
use Magento\Quote\Model\{Quote, QuoteFactory};
use Magento\Quote\Model\Quote\Address;
use Magento\SalesRule\Model\CouponFactory;
use Simpler\Checkout\Api\Data\{AddressInterface, ItemInterface};
use Simpler\Checkout\Exception\{
    InapplicableCouponException,
    InvalidCouponException,
    InvalidProductException,
    OutOfStockProductException,
    UnavailableProductException
};
use Simpler\Checkout\Helper\{SimplerConfig, StockHelper};

class Cart
{
    /**
     * @var SimplerConfig
     */
    private $simplerConfig;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;
    /**
     * @var AddressInterfaceFactory
     */
    private $addressFactory;
    /**
     * @var CouponFactory
     */
    private $couponFactory;
    /**
     * @var GuestCartManagementInterface
     */
    private $guestCartManagement;
    /**
     * @var CouponManagementInterface
     */
    private $couponManagement;
    /**
     * @var MaskedQuoteIdToQuoteId
     */
    private $maskedQuoteIdToQuoteId;
    /**
     * @var QuoteFactory
     */
    private $quoteFactory;
    /**
     * @var StockHelper
     */
    private $stockHelper;
    /**
     * @var ProductHelper
     */
    private $productHelper;
    /**
     * @var Configurable
     */
    private $configurableType;

    public function __construct(
        SimplerConfig                $simplerConfig,
        ManagerInterface             $eventManager,
        CartRepositoryInterface      $cartRepository,
        ProductRepositoryInterface   $productRepository,
        AddressInterfaceFactory      $addressFactory,
        CouponFactory                $couponFactory,
        GuestCartManagementInterface $guestCartManagement,
        CouponManagementInterface    $couponManagement,
        MaskedQuoteIdToQuoteId       $maskedQuoteIdToQuoteId,
        QuoteFactory                 $quoteFactory,
        StockHelper                  $stockHelper,
        ProductHelper                $productHelper,
        Configurable $configurableType
    ) {
        $this->simplerConfig = $simplerConfig;
        $this->eventManager = $eventManager;
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;
        $this->addressFactory = $addressFactory;
        $this->couponFactory = $couponFactory;
        $this->guestCartManagement = $guestCartManagement;
        $this->couponManagement = $couponManagement;
        $this->maskedQuoteIdToQuoteId = $maskedQuoteIdToQuoteId;
        $this->quoteFactory = $quoteFactory;
        $this->stockHelper = $stockHelper;
        $this->productHelper = $productHelper;
        $this->configurableType = $configurableType;
    }

    /**
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     */
    public function createQuote(): \Magento\Quote\Model\Quote
    {
        $maskedId = $this->guestCartManagement->createEmptyCart();
        $newCartId = $this->maskedQuoteIdToQuoteId->execute($maskedId);
        return $this->quoteFactory->create()->loadActive($newCartId)->setData('is_simpler_order', true);
    }

    /**
     * Adds given products to quote.
     * The magento core will cache a lot of stuff with the assertion that each request will add a single request to a quote.
     * To avoid these pitfalls we're iterating through the products array, adding each one to a new cart every time.
     * We'll then collect the built Quote\Item instances and add them to the final quote using the `addItem` instead of `addProduct`
     *
     * @param  ItemInterface[] $items
     * @param  Quote $quote
     * @throws OutOfStockProductException
     * @throws UnavailableProductException
     * @throws InvalidProductException
     */
    public function addProducts(array $items, Quote &$quote)
    {
        $this->productHelper->setSkipSaleableCheck(true);
        $quoteItems = [];

        foreach ($items as $item) {
            try {
                /** @var Product $product */
                $product = $this->productRepository->get($item->getId());
            } catch (NoSuchEntityException $e) {
                throw new UnavailableProductException(sprintf('Product %s does not exist.', $item->getId()));
            }
            if ($product->isDisabled()) {
                throw new UnavailableProductException(sprintf('Product %s is disabled.', $item->getId()));
            }
            if (!$product->isSalable()) {
                throw new UnavailableProductException(sprintf('Product %s is not saleable', $item->getId()));
            }
            if (!$this->isSupportedProduct($product)) {
                throw new InvalidProductException(sprintf(
                    'Product %s is of unsupported type %s.',
                    $product->getSku(),
                    $product->getTypeId()
                ));
            }
            if ($this->stockHelper->getStockQuantity($product) <= 0) {
                throw new OutOfStockProductException(sprintf('Product %s is out of stock.', $product->getSku()));
            }

            try {
                $q = $this->quoteFactory->create();
                $addRequest = new DataObject([
                    'qty' => $item->getQuantity(),
                    'product' => $product->getId(),
                ]);

                $options = [];
                $requested = $item->getCustomValues() ?? [];
                foreach ($requested as $attr) {
                    $options[$attr->getKey()] = $attr->getValue();
                }
                $addRequest->setData('options', $options);

                $this->eventManager->dispatch(
                    'checkout_cart_product_add_before',
                    ['info' => $addRequest, 'product' => $product]
                );
                $result = $this->addProductToQuote($q, $product, $addRequest);
                $this->eventManager->dispatch(
                    'checkout_cart_product_add_after',
                    ['quote_item' => $result, 'product' => $product]
                );
                $quoteItems = array_merge($quoteItems, $q->getAllVisibleItems());
            } catch (LocalizedException $exception) {
                throw new UnavailableProductException('Product could not be added to cart: ' . $exception->getMessage());
            }
        }

        foreach ($quoteItems as $item) {
            $quote->addItem($item);
            foreach ($item->getChildren() as $child) {
                $quote->addItem($child);
            }
        }
        $quote = $this->updateQuoteTotals($quote);
    }

    private function addProductToQuote(Quote $quote, $product, $addRequest)
    {
        $parentIds = $this->configurableType->getParentIdsByChild($product->getId());
        if (empty($parentIds)) {
            return $quote->addProduct($product, $addRequest);
        }

        $parent = $this->productRepository->getById(array_first($parentIds));
        $configurableAttributes = $this->configurableType->getConfigurableAttributesAsArray($parent);

        // Prepare the superAttribute array
        $superAttributes = [];

        foreach ($configurableAttributes as $attribute) {
            $attributeId = $attribute['attribute_id'];
            $attributeCode = $attribute['attribute_code'];
            $attributeValue = $product->getData($attributeCode);

            if ($attributeValue) {
                $superAttributes[$attributeId] = $attributeValue;
            }
        }

        $addRequest->setData('super_attribute', $superAttributes);
        return $quote->addProduct($parent, $addRequest);
    }

    /**
     * Converts given data to Magento\Quote\Model\Quote\Address.
     *
     * @param  AddressInterface  $address
     * @return ?Address
     */
    public function addressToMagentoShippingAddress(?AddressInterface $address): ?Address
    {
        if (is_null($address)) {
            return null;
        }

        $addressData = $address->toMagentoQuoteAddressArray();
        $addressData['address_type'] = AbstractAddress::TYPE_SHIPPING;
        $addressData['customer_address_id'] = null;

        return $this->addressFactory->create()->addData($addressData);
    }

    /**
     * Converts given data to Magento\Quote\Model\Quote\Address.
     *
     * @param  AddressInterface  $address
     * @return Address
     */
    public function addressToMagentoBillingAddress(?AddressInterface $address): Address
    {
        if (is_null($address)) {
            return $this->addressFactory->create()
                ->addData([
                    'address_type' => AbstractAddress::TYPE_BILLING,
                    'customer_address_id' => null,
                    'firstname' => 'N/A',
                    'lastname' => 'N/A',
                    'telephone' => 'N/A',
                    'street' => 'N/A',
                    'city' => 'N/A',
                    'postcode' => 'N/A',
                    'country_id' => 'GR'
                ])
                ->setShouldIgnoreValidation(true);
        }

        $addressData = $address->toMagentoQuoteAddressArray();
        $addressData['address_type'] = AbstractAddress::TYPE_BILLING;
        $addressData['customer_address_id'] = null;

        return $this->addressFactory->create()->addData($addressData);
    }

    private function isSupportedProduct(ProductInterface $product): bool
    {
        return $product->getTypeId() === Type::TYPE_SIMPLE || $product->getTypeId() === Type::TYPE_VIRTUAL;
    }

    /**
     * Collect and update quote totals.
     * @param Quote $quote
     */
    public function updateQuoteTotals(Quote $quote)
    {
        $quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
        $quote->setTotalsCollectedFlag(false);
        $this->eventManager->dispatch('simpler_checkout_before_collect_totals', [
            'quote' => $quote
        ]);
        $quote->collectTotals();
        $this->cartRepository->save($quote);
        return $this->quoteFactory->create()->loadActive($quote->getId());
    }

    /**
     * @param ?string $code
     * @param Quote $quote
     * @throws Exception
     * @throws InvalidCouponException
     * @throws InapplicableCouponException
     */
    public function maybeApplyCouponCode(?string $code, Quote &$quote)
    {
        if ($code == null) {
            return;
        }

        $coupon = $this->couponFactory->create()->loadByCode($code);
        if (!$coupon->getCode() || mb_strtolower($coupon->getCode()) != mb_strtolower($code)) {
            throw new InvalidCouponException("Coupon $code does not exist");
        }

        try {
            $this->couponManagement->set($quote->getId(), $code);
            $quote->setCouponCode($code);
        } catch (\Exception $e) {
            throw new InapplicableCouponException($e->getMessage());
        }
    }

    /**
     * @param  ShippingMethodInterface  $shippingMethod
     * @return string
     */
    public static function getShippingMethodId(ShippingMethodInterface $shippingMethod): string
    {
        return "{$shippingMethod->getCarrierCode()}_{$shippingMethod->getMethodCode()}";
    }

    /**
     * @param  ShippingMethodInterface  $shippingMethod
     * @return string
     */
    public static function getShippingMethodName(ShippingMethodInterface $shippingMethod): string
    {
        return "{$shippingMethod->getCarrierTitle()} - {$shippingMethod->getMethodTitle()}";
    }
}
