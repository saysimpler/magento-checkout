<?php

namespace Simpler\Checkout\Model\Processors;

use Magento\Quote\Model\Quote;
use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\SalesRule\Model\RuleRepository;
use Simpler\Checkout\Api\Data\FeeInterfaceFactory;
use Simpler\Checkout\Helper\Money;
use Simpler\Checkout\Api\Data\ItemQuoteResponseInterface;

class DiscountCollector implements DiscountCollectorInterface
{
    /**
     * @var FeeInterfaceFactory
     */
    private $feeFactory;
    /**
     * @var RuleRepository
     */
    private $ruleRepository;

    public function __construct(
        FeeInterfaceFactory $feeFactory,
        RuleRepository $ruleRepository
    ) {
        $this->feeFactory = $feeFactory;
        $this->ruleRepository = $ruleRepository;
    }

    /**
     * @param Magento\Quote\Model\Quote $magentoQuote
     * @param ItemQuoteResponseInterface $simplerQuote
     */
    public function collect(Quote $magentoQuote, ItemQuoteResponseInterface $simplerQuote)
    {
        $simplerQuote->setFees([]);
        $totalCouponDiscount = 0;
        $perRuleDiscount = [];
        foreach ($magentoQuote->getAllVisibleItems() as $quoteItem) {
            try {
                $discounts = [];
                if (method_exists($quoteItem->getExtensionAttributes(), 'getDiscounts')) {
                    $discounts = $quoteItem->getExtensionAttributes()->getDiscounts() ?? [];
                }
                foreach ($discounts as $discount) {
                    try {
                        $rule = $this->ruleRepository->getById($discount->getRuleId());
                        $dd = $discount->getDiscountData();
                        if ($rule != NULL && $dd != NULL && $dd->getAmount() != NULL) {
                            if ($rule->getCouponType() == RuleInterface::COUPON_TYPE_SPECIFIC_COUPON) {
                                $totalCouponDiscount = $totalCouponDiscount + $dd->getAmount();
                            } else if ($rule->getCouponType() == RuleInterface::COUPON_TYPE_NO_COUPON) {
                                if (isset($perRuleDiscount[$rule->getName()])) {
                                    $perRuleDiscount[$rule->getName()] += $dd->getAmount();
                                } else {
                                    $perRuleDiscount[$rule->getName()] = $dd->getAmount();
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            } catch (\Exception $e) {
                continue;
            }
        }
        $simplerQuote->setDiscountCents(Money::toCents($totalCouponDiscount));
        foreach ($perRuleDiscount as $name => $amount) {
            $fee = $this->feeFactory->create()->setTitle($name)->setTotalCents(Money::toCents($amount * -1.0));
            $simplerQuote->addFee($fee);
        }
    }
}
