<?php

namespace Simpler\Checkout\Model\Processors;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Framework\DB\Transaction;
use Magento\Framework\Exception\{
    CouldNotSaveException,
    InputException,
    LocalizedException,
    NoSuchEntityException
};
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\{Quote, QuoteManagement};
use Magento\Sales\Api\Data\{OrderInterface, OrderStatusHistoryInterfaceFactory};
use Magento\Sales\Api\{InvoiceRepositoryInterface, OrderManagementInterface, OrderRepositoryInterface};
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Store\Model\App\Emulation;
use Simpler\Checkout\Api\Data\OrderBoxNowInterface;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterfaceFactory;
use Simpler\Checkout\Api\Data\SimplerDiscountInterface;
use Simpler\Checkout\Api\Data\SubmitRequestInterface;
use Simpler\Checkout\Exception\{
    InvalidProductException,
    OutOfStockProductException,
    UnavailableProductException
};
use Simpler\Checkout\Api\OrderExtensionAttributesRepositoryInterface;
use Simpler\Checkout\Event\Submit\{
    AfterCustomerAssignedEvent,
    AfterCustomerInvoiceCreated,
    AfterOrderCreatedEvent,
    AfterOrderNoteCreatedEvent,
    AfterProductsAddedEvent,
    AfterQuoteCreatedEvent,
    AfterShippingMethodAddedEvent,
    AfterTotalsCollectedEvent,
    BeforeCustomerAssignedEvent,
    BeforeProductsAddedEvent,
    BeforeTotalsCollectedEvent
};
use Simpler\Checkout\Event\Event;
use Simpler\Checkout\Gateway\Config\ConfigProvider as PaymentProvider;
use Simpler\Checkout\Helper\SimplerConfig;

class Submit
{
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var Cart
     */
    private $cartProcessor;
    /**
     * @var QuoteManagement
     */
    private $quoteManagement;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var CustomerInterfaceFactory
     */
    private $customerFactory;
    /**
     * @var OrderManagementInterface
     */
    private $orderManagement;
    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;
    /**
     * @var InvoiceService
     */
    private $invoiceService;
    /**
     * @var Transaction
     */
    private $transaction;
    /**
     * @var OrderStatusHistoryInterfaceFactory
     */
    private $orderStatusHistoryFactory;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var Quote
     */
    private $quote;
    /**
     * @var ManagerInterface
     */
    private $eventManager;
    /**
     * @var OrderExtensionAttributesRepositoryInterface
     */
    private $orderExtensionAttributesRepository;
    /**
     * @var OrderExtensionAttributesInterfaceFactory
     */
    private $orderExtensionAttributesInterfaceFactory;
    /**
     * @var OrderExtensionAttributesInterface
     */
    private $simplerOrderExtensionAttributes;

    /**
     * @var SimplerConfig
     */
    private $simplerConfig;

    public function __construct(
        CartRepositoryInterface            $cartRepository,
        Cart                               $cartProcessor,
        QuoteManagement                    $quoteManagement,
        CustomerRepositoryInterface        $customerRepository,
        CustomerInterfaceFactory           $customerFactory,
        OrderManagementInterface           $orderManagement,
        InvoiceService                     $invoiceService,
        InvoiceRepositoryInterface         $invoiceRepository,
        Transaction                        $transaction,
        OrderStatusHistoryInterfaceFactory $orderStatusHistoryFactory,
        OrderRepositoryInterface           $orderRepository,
        Emulation                          $emulation,
        ManagerInterface                   $eventManager,
        OrderExtensionAttributesInterfaceFactory $orderExtensionAttributesInterfaceFactory,
        OrderExtensionAttributesRepositoryInterface $orderExtensionAttributesRepository,
        SimplerConfig                      $simplerConfig
    ) {
        $this->cartRepository = $cartRepository;
        $this->cartProcessor = $cartProcessor;
        $this->quoteManagement = $quoteManagement;
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->orderManagement = $orderManagement;
        $this->invoiceService = $invoiceService;
        $this->invoiceRepository = $invoiceRepository;
        $this->transaction = $transaction;
        $this->orderStatusHistoryFactory = $orderStatusHistoryFactory;
        $this->orderRepository = $orderRepository;
        $this->emulation = $emulation;
        $this->eventManager = $eventManager;
        $this->orderExtensionAttributesRepository
            = $orderExtensionAttributesRepository;
        $this->orderExtensionAttributesInterfaceFactory
            = $orderExtensionAttributesInterfaceFactory;
        $this->simplerConfig = $simplerConfig;
    }

    /**
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws InputMismatchException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws InvalidProductException
     * @throws OutOfStockProductException
     * @throws UnavailableProductException
     */
    public function submit(SubmitRequestInterface $request): string
    {
        $this->emulation->startEnvironmentEmulation($request->getStoreId());

        if (($existing = $this->getOrderExtensionAttributesBySimplerId($request->getSimplerOrderId())) !== null) {
            return $existing->getOrderId();
        }

        $this->quote = $this->cartProcessor->createQuote();
        /**
         * @deprecated This event is replaced by AfterQuoteCreatedEvent and will be removed in a future release.
         * @see AfterQuoteCreatedEvent
         */
        $this->eventManager->dispatch('simpler_checkout_after_quote_creation', ['quote' => $this->quote]);
        $this->dispatch(new AfterQuoteCreatedEvent($this->quote));

        $this->dispatch(new BeforeProductsAddedEvent($this->quote, $request->getItems()));
        $this->cartProcessor->addProducts($request->getItems(), $this->quote);
        $this->dispatch(new AfterProductsAddedEvent($this->quote, $request->getItems()));

        // Todo setting currency is not working. Even though we are passing "EUR" in request body, quote & order are created with "USD" (default).
        $this->quote->setQuoteCurrencyCode($request->getCurrency());

        $this->dispatch(
            new BeforeCustomerAssignedEvent(
                $this->quote,
                $customer = $this->convertCustomer($request),
                $shippingAddress = $this->cartProcessor->addressToMagentoShippingAddress($request->getShippingTo()),
                $billingAddress = $this->cartProcessor->addressToMagentoBillingAddress($request->getShippingTo())
            )
        );
        $this->quote->assignCustomerWithAddressChange(
            $customer,
            $billingAddress,
            $shippingAddress
        );
        $this->dispatch(
            new AfterCustomerAssignedEvent(
                $this->quote,
                $customer,
                $shippingAddress,
                $billingAddress
            )
        );
        $this->cartRepository->save($this->quote);

        $pm = $request->getPaymentMethod() ?? PaymentProvider::CODE;
        $this->quote->setPaymentMethod($pm);
        $this->quote->getPayment()->importData(['method' => $pm]);

        $this->cartProcessor->maybeApplyCouponCode($request->getCoupon(), $this->quote);
        $this->quote->getShippingAddress()->setCollectShippingRates(true)->collectShippingRates();
        $this->quote->setTotalsCollectedFlag(false)->collectTotals();

        $this->quote->getShippingAddress()->setShippingMethod($request->getShippingMethod());

        if ($request->getBoxNow() && $request->getShippingMethod() == 'boxnow_boxnow') {
            $this->setBoxNow($request->getBoxNow());
        }

        $pickupStoreId = $request->getShippingTo() ? $request->getShippingTo()->getPickupStoreId() : null;
        $this->eventManager->dispatch('simpler_checkout_after_shipping_method_selected', [
            'quote' => $this->quote,
            'store_id' => $pickupStoreId,
            'request' => $request
        ]);
        //todo: deprecate
        $this->dispatch(new AfterShippingMethodAddedEvent($this->quote, $request->getShippingMethod(), $pickupStoreId));

        $this->quote->setCustomerIsGuest(false);

        $this->maybeApplySimplerDiscount($request->getSimplerDiscount());

        $this->dispatch(new BeforeTotalsCollectedEvent($this->quote));
        $this->cartProcessor->updateQuoteTotals($this->quote);
        $this->dispatch(new AfterTotalsCollectedEvent($this->quote));

        $this->eventManager->dispatch('simpler_checkout_before_order_placed', [
            'quote' => $this->quote,
            'request' => $request,
        ]);
        $orderId = $this->quoteManagement->placeOrder($this->quote->getId());

        $order = $this->orderRepository->get($orderId);

        if ($order && $order->getEntityId()) {
            $applicableOrderStatus = $this->simplerConfig->getConfig(SimplerConfig::APPLICABLE_ORDER_STATUS);
            if($applicableOrderStatus){
                $order->setState($applicableOrderStatus)
                    ->setStatus($applicableOrderStatus);
            }
        }
        //TODO deprecate
        $this->dispatch(new AfterOrderCreatedEvent($order, $request->getPaymentMethod()));
        $this->eventManager->dispatch('simpler_checkout_after_order_placed', [
            'order' => $order,
            'request' => $request
        ]);

        if ($simplerOrderID = $request->getSimplerOrderId()) {
            $this->getSimplerOrderExtensionAttributes()->setSimplerOrderId($simplerOrderID);
        }
        $this->invoice($order);

        if ($request->getNote()) {
            $orderStatusHistory = $this->orderStatusHistoryFactory->create()
                ->setStatus($order->getStatus())
                ->setParentId($order->getEntityId())
                ->setComment($request->getNote())
                ->setIsCustomerNotified(false)
                ->setIsVisibleOnFront(false);
            $this->orderManagement->addComment($order->getEntityId(), $orderStatusHistory);
            $this->dispatch(new AfterOrderNoteCreatedEvent($order, $request->getNote()));
        }

        if ($request->getInvoice() !== null) {
            $this->getSimplerOrderExtensionAttributes()->setInvoice($request->getInvoice());
            $this->dispatch(new AfterCustomerInvoiceCreated($order, $request->getInvoice()));
        }

        $order->getExtensionAttributes()->setSimplerCheckout($this->getSimplerOrderExtensionAttributes());

        $this->orderRepository->save($order);
        $this->emulation->stopEnvironmentEmulation();
        return $order->getIncrementId();
    }

    /**
     * @param  SubmitRequestInterface  $req
     * @return CustomerInterface
     * @throws NoSuchEntityException
     * @throws InputException
     * @throws LocalizedException
     * @throws InputMismatchException
     */
    private function convertCustomer(SubmitRequestInterface $req): CustomerInterface
    {
        $user = $req->getUser();
        try {
            return $this->customerRepository->get($user->getEmail());
        } catch (NoSuchEntityException $e) {
            $customer = $this->customerFactory->create();
            $customer->setFirstname($user->getFirstName())
                ->setLastname($user->getLastName())
                ->setEmail($user->getEmail());
            return $this->customerRepository->save($customer);
        }
    }

    /**
     * @param  OrderInterface  $order
     * @return Invoice
     * @throws LocalizedException
     */
    private function invoice(OrderInterface $order)
    {
        if ($order->canInvoice()) {
            if ($order->getState() == Order::STATE_NEW) {
                $order->setState(Order::STATE_PROCESSING)->setStatus(Order::STATE_PROCESSING);
            }
            $order->setData("is_simpler_order", true);
            $invoice = $this->invoiceService->prepareInvoice($order);
            $invoice->register();
            $invoice->setData("is_simpler_order", true);

            $invoice->setDiscountAmount($order->getDiscountAmount());
            $invoice->setDiscountDescription($order->getDiscountDescription());
            $invoice->setGrandTotal($order->getGrandTotal());

            $order->setBaseTotalPaid($order->getBaseGrandTotal());
            $order->setTotalPaid($order->getGrandTotal());

            $this->invoiceRepository->save($invoice);
            $this->transaction->addObject($invoice)->addObject($invoice->getOrder())->save();

            return $invoice;
        }

        return null;
    }

    private function maybeApplySimplerDiscount(?SimplerDiscountInterface $discount)
    {
        if ($discount && $discount->getAmount()) {
            $this->quote->setData('simpler_discount_cents', $discount->getAmount());
        }
    }

    private function dispatch(Event $event)
    {
        $this->eventManager->dispatch(
            $event->getName(),
            $event->toDispatchable()
        );
    }
    private function getOrderExtensionAttributesBySimplerId($simplerOrderId): ?OrderExtensionAttributesInterface
    {
        if (!$simplerOrderId) {
            return null;
        }

        try {
            return $this->orderExtensionAttributesRepository->getBy(OrderExtensionAttributesInterface::SIMPLER_ORDER_ID, $simplerOrderId);
        } catch (NoSuchEntityException $exception) {
            return null;
        }
    }

    private function getSimplerOrderExtensionAttributes(): OrderExtensionAttributesInterface
    {
        return $this->simplerOrderExtensionAttributes ?: $this->simplerOrderExtensionAttributes = $this->orderExtensionAttributesInterfaceFactory->create();
    }

    private function setBoxNow(OrderBoxNowInterface $boxNow)
    {
        $data = json_encode([
            'boxnowLockerId'           => $boxNow->getLockerId(),
            'boxnowLockerAddressLine1' => $boxNow->getAddress(),
            'boxnowLockerPostalCode'   => $boxNow->getPostcode(),
        ]);

        $this->quote
            ->getShippingAddress()
            ->setBoxnow_id($data)
            ->setBoxnowId($data);

        $this->quote
            ->getBillingAddress()
            ->setBoxnow_id($data)
            ->setBoxnowId($data);
    }
}
