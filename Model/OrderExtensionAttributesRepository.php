<?php

namespace Simpler\Checkout\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Config\Dom\ValidationException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesSearchResultsInterfaceFactory;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterfaceFactory;
use Simpler\Checkout\Api\OrderExtensionAttributesRepositoryInterface;
use Simpler\Checkout\Model\ResourceModel\OrderExtensionAttributesResource;
use Simpler\Checkout\Model\ResourceModel\OrderExtensionAttributesCollectionFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Simpler\Checkout\Api\Data\OrderInvoiceInterfaceFactory;

class OrderExtensionAttributesRepository implements OrderExtensionAttributesRepositoryInterface
{
    /**
     * @var OrderExtensionAttributes
     */
    private $resource;
    /**
     * @var OrderExtensionAttributesInterfaceFactory
     */
    private $extensionsFactory;
    /**
     * @var OrderExtensionAttributesCollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var OrderExtensionAttributesSearchResultsInterfaceFactory
     */
    private $orderExtensionAttributesSearchResultsInterfaceFactory;
    /**
     * @var OrderInvoiceInterfaceFactory
     */
    private $invoiceFactory;
    /**
     * @var Json
     */
    private $json;

    public function __construct(
        OrderExtensionAttributesResource $orderExtensionAttributes,
        OrderExtensionAttributesInterfaceFactory $orderExtensionAttributesInterfaceFactory,
        OrderExtensionAttributesCollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        OrderExtensionAttributesSearchResultsInterfaceFactory $orderExtensionAttributesSearchResultsInterfaceFactory,
        OrderInvoiceInterfaceFactory $orderInvoiceInterfaceFactory,
        Json $json
    ) {
        $this->resource = $orderExtensionAttributes;
        $this->extensionsFactory = $orderExtensionAttributesInterfaceFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->orderExtensionAttributesSearchResultsInterfaceFactory
            = $orderExtensionAttributesSearchResultsInterfaceFactory;

        $this->invoiceFactory = $orderInvoiceInterfaceFactory;
        $this->json = $json;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getBy(string $column, $value)
    {
        /** @var OrderExtensionAttributesInterface $extensionAttributes */
        $extensionAttributes = $this->extensionsFactory->create();
        $this->resource->load($extensionAttributes, $value, $column);
        if (! $extensionAttributes->getId()) {
            throw new NoSuchEntityException(__(
                'Unable to find simpler order extension attributes by "%1"="%2"',
                $column,
                $value
            ));
        }


        // Convert JSON to OrderInvoice
        if (($invoice = $extensionAttributes->getInvoice()) && is_string($invoice)) {
            $extensionAttributes->setInvoice(
                $this->invoiceFactory->create(['data' => $this->json->unserialize($invoice)])
            );
        }

        return $extensionAttributes;
    }


    /**
     * @throws CouldNotSaveException
     */
    public function save(OrderExtensionAttributesInterface $extensionAttributes)
    {
        // Convert OrderInvoice to JSON
        if ($extensionAttributes->getInvoice()) {
            $extensionAttributes->setData(
                OrderExtensionAttributesInterface::INVOICE,
                $this->json->serialize($extensionAttributes->getInvoice()->toArray())
            );
        }

        try {
            $this->resource->save($extensionAttributes);
        } catch (ValidationException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Unable to save model %1', $extensionAttributes->getId()));
        }

        return $extensionAttributes;
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->orderExtensionAttributesSearchResultsInterfaceFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->count());
        return $searchResults;
    }

    public function deleteBy(string $column, $value)
    {
        /** @var OrderExtensionAttributesInterface $extensionAttributes */
        $extensionAttributes = $this->extensionsFactory->create();
        $this->resource->load($extensionAttributes, $value, $column);
        $this->resource->delete($extensionAttributes);
    }
}
