<?php

namespace Simpler\Checkout\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface;


class OrderExtensionAttributesCollection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = OrderExtensionAttributesInterface::ID;

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Simpler\Checkout\Model\Api\OrderExtensionAttributes::class, OrderExtensionAttributesResource::class);
    }
}
