<?php

namespace Simpler\Checkout\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Simpler\Checkout\Api\Data\OrderExtensionAttributesInterface;

class OrderExtensionAttributesResource extends AbstractDb
{
    const TABLE = 'simpler_checkout_order_extension_attributes';

    protected function _construct()
    {
        $this->_init(self::TABLE, OrderExtensionAttributesInterface::ID);
    }
}
