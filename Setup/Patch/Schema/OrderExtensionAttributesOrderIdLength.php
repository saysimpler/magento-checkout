<?php

namespace Simpler\Checkout\Setup\Patch\Schema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class OrderExtensionAttributesOrderIdLength implements SchemaPatchInterface, PatchVersionInterface {

    /**
     * @var SchemaSetupInterface
     */
    private $schemaSetup;

    public function __construct(
        SchemaSetupInterface $schemaSetup
    ) {
        $this->schemaSetup = $schemaSetup;
    }
    
    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $this->schemaSetup->startSetup();

        $table = $this->schemaSetup->getTable('simpler_checkout_order_extension_attributes');
        $this->schemaSetup->getConnection()->dropForeignKey($table, 'SIMPLER_CHECKOUT_ORDER_EXTENSION_ATTRIBUTES');
         $this->schemaSetup->getConnection()->changeColumn(
             $table,
            'order_id',
            'order_id',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 50,
                'nullable' => true,
                'comment' => 'Magento Order Id'
            ]
        );
        $this->schemaSetup->endSetup();
    }

    public static function getVersion()
    {
        // If the version of the module is higher than or equal to the version specified in your patch, then the patch is skipped.
        // If the version in the database is lower, then the patch installs.
        return '2.4.5';
    }
}
