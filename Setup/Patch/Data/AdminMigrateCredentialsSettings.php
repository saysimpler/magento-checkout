<?php

namespace Simpler\Checkout\Setup\Patch\Data;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AdminMigrateCredentialsSettings implements DataPatchInterface
{

    /**
     * @var ScopeConfigInterface
     */
    private $configReader;
    /**
     * @var WriterInterface
     */
    private $configWriter;

    public function __construct(ScopeConfigInterface $configReader, WriterInterface $configWriter)
    {
        $this->configReader = $configReader;
        $this->configWriter = $configWriter;
    }

    public function apply()
    {
        $prev = $this->configReader->getValue('simpler_checkout/settings/merchant_id');
        if ($prev !== null) {
            $this->configWriter->save('simpler_checkout_credentials/live/merchant_id', $prev);
        }

        $prev = $this->configReader->getValue('simpler_checkout/settings/app_secret');
        if ($prev !== null) {
            $this->configWriter->save('simpler_checkout_credentials/live/app_secret', $prev);
        }
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }
}
