<?php

namespace Simpler\Checkout\Setup\Patch\Data;

use Magento\Integration\Model\ConfigBasedIntegrationManager;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class UpdateSimplerIntegrationAcl implements DataPatchInterface
{


    /**
     * @var ConfigBasedIntegrationManager
     */
    private $integrationManager;

    public function __construct(ConfigBasedIntegrationManager $integrationManager)
    {
        $this->integrationManager = $integrationManager;
    }

    public function apply()
    {
        $this->integrationManager->processIntegrationConfig(['SimplerIntegration']);
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }
}