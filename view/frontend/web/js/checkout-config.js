define([
  'ko', 'jquery', 'Magento_Customer/js/customer-data'
], function (ko, $, customerData) {
  var checkoutConfig = ko.observable(null);
  var subscription = null;

  var getConfig = function (config) {
    return $.ajax({
      url: config.configUrl,
      type: 'GET',
      dataType: 'json'
    }).done(function (data) {
      checkoutConfig(data);
    }).fail(function (err) {
      console.error(err);
    });
  }

  return function (config, callback) {
    checkoutConfig.subscribe(callback);

    if (checkoutConfig() == null) {
      getConfig(config);
    }

    if (subscription == null) {
      subscription = customerData.get('cart').subscribe(function () {
        customerData.invalidate(['cart']);
        return getConfig(config);
      });
    }
  }
})
