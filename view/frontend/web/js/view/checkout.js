define([
  'uiComponent', 'jquery', 'ko', 'Simpler_Checkout/js/checkout-config',
], function (Component, $, ko, configFactory) {
  'use strict';
  return Component.extend({
    defaults: {
      template: 'Simpler_Checkout/checkout',
      shouldRender: ko.observable(false),
      checkoutAttrs: ko.observable({}),
    },
    initialize: function (config) {
      var self = this;
      this._super();
      configFactory(config, function (data) {
        self.checkoutAttrs($.extend({
          position: self.position
        }, data.attrs));
        self.shouldRender(data.shouldRender && data.enabledPositions[self.position]);
      })
    }
  })
});
