<?php

namespace Simpler\Checkout\Event\Submit;

use Magento\Quote\Model\Quote;
use Simpler\Checkout\Event\Event;

class AfterShippingMethodAddedEvent extends Event
{
    protected $name = 'simpler_checkout_submit_quote_after_shipping_method_added';

    /**
     * @var string|null
     */
    private $shippingMethod;
    /**
     * @var Quote
     */
    private $quote;
    /**
     * @var string|null
     */
    private $pickupStoreId;

    public function __construct(Quote $quote, ?string $shippingMethod, ?string $pickupStoreId)
    {
        $this->shippingMethod = $shippingMethod;
        $this->quote          = $quote;
        $this->pickupStoreId  = $pickupStoreId;
    }

    /**
     * @return string|null
     */
    public function getPickupStoreId(): ?string
    {
        return $this->pickupStoreId;
    }

    /**
     * @return Quote
     */
    public function getQuote(): Quote
    {
        return $this->quote;
    }

    /**
     * @return string|null
     */
    public function getShippingMethod(): ?string
    {
        return $this->shippingMethod;
    }
}
