<?php

namespace Simpler\Checkout\Event\Submit;

use Magento\Sales\Api\Data\OrderInterface;
use Simpler\Checkout\Event\Event;

class AfterOrderNoteCreatedEvent extends Event
{
    protected $name = 'simpler_checkout_submit_after_order_note_created';
    /**
     * @var string
     */
    private $note;
    /**
     * @var OrderInterface
     */
    private $order;

    public function __construct(OrderInterface $order, string $note)
    {
        $this->note  = $note;
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getNote(): string
    {
        return $this->note;
    }

    /**
     * @return OrderInterface
     */
    public function getOrder(): OrderInterface
    {
        return $this->order;
    }
}
