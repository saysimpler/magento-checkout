<?php

namespace Simpler\Checkout\Event\Submit;

use Magento\Sales\Api\Data\OrderInterface;
use Simpler\Checkout\Api\Data\OrderInvoiceInterface;
use Simpler\Checkout\Event\Event;

class AfterCustomerInvoiceCreated extends Event
{
    protected $name = 'simpler_checkout_submit_after_customer_invoice_created';

    /**
     * @var OrderInterface
     */
    private $order;
    /**
     * @var OrderInvoiceInterface
     */
    private $invoice;

    public function __construct(
        OrderInterface $order,
        OrderInvoiceInterface $invoice
    ) {
        $this->order   = $order;
        $this->invoice = $invoice;
    }

    /**
     * @return OrderInterface
     */
    public function getOrder(): OrderInterface
    {
        return $this->order;
    }

    /**
     * @return OrderInvoiceInterface
     */
    public function getInvoice(): OrderInvoiceInterface
    {
        return $this->invoice;
    }
}
