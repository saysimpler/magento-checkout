<?php

namespace Simpler\Checkout\Event\Submit;

use Magento\Sales\Model\Order;
use Simpler\Checkout\Event\Event;

class AfterOrderCreatedEvent extends Event
{
    protected $name = 'simpler_checkout_submit_order_after_created';

    /**
     * @var Order
     */
    private $order;

    /**
     * @var ?string
     */
    private $paymentMethod;

    public function __construct(Order $order, ?string $paymentMethod)
    {
        $this->order = $order;
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return ?string
     */
    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }
}
