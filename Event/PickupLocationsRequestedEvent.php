<?php

namespace Simpler\Checkout\Event;

use Simpler\Checkout\Api\Data\PickupLocationsInterface;

class PickupLocationsRequestedEvent extends Event
{
    protected $name = 'simpler_checkout_pickup_locations_requested';
    /**
     * @var PickupLocationsInterface
     */
    private $pickupLocations;

    public function __construct(PickupLocationsInterface $pickupLocations)
    {
        $this->pickupLocations = $pickupLocations;
    }

    /**
     * @return PickupLocationsInterface
     */
    public function getPickupLocations(): PickupLocationsInterface
    {
        return $this->pickupLocations;
    }
}
