<?php

namespace Simpler\Checkout\Event;

abstract class Event
{
    // The data array key for resolving a Simpler Checkout event class.
    const KEY = 'simpler_checkout_event';

    /**
     * Event's unique name. Must be overwritten by children.
     *
     * @var string
     */
    protected $name = '';

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Convert Event to the format expect by Magento's EventDispatcher.
     *
     * @return array
     */
    public function toDispatchable(): array
    {
        return [
            self::KEY => $this
        ];
    }
}
