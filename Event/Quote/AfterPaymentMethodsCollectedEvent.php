<?php

namespace Simpler\Checkout\Event\Quote;

use Magento\Quote\Model\Quote;
use Simpler\Checkout\Api\Data\PaymentMethodInterface;
use Simpler\Checkout\Event\Event;

class AfterPaymentMethodsCollectedEvent extends Event
{
    protected $name = 'simpler_checkout_quote_after_payment_methods_collected';

    /**
     * @var Quote
     */
    private $quote;
    /**
     * @var PaymentMethodInterface[]
     */
    private $methods;

    public function __construct(Quote $quote, array $methods = [])
    {
        $this->quote = $quote;
        $this->methods = $methods;
    }

    /**
     * @return PaymentMethodInterface[]
     */
    public function getPaymentMethods(): array
    {
        return $this->methods;
    }

    /**
     * @return Quote
     */
    public function getQuote(): Quote
    {
        return $this->quote;
    }
}
