<?php

namespace Simpler\Checkout\Event\Quote;

use Magento\Quote\Model\Quote;
use Simpler\Checkout\Api\Data\ItemQuoteResponseInterface;
use Simpler\Checkout\Event\Event;

class AfterDiscountsCollectedEvent extends Event
{
    protected $name = 'simpler_checkout_quote_after_discounts_collected';

    /**
     * @var Quote
     */
    private $quote;
    /**
     * @var ItemQuoteResponseInterface
     */
    private $simplerQuote;

    public function __construct(
        Quote $quote,
        ItemQuoteResponseInterface $simplerQuote
    ) {
        $this->quote = $quote;
        $this->simplerQuote = $simplerQuote;
    }

    /**
     * @return Quote
     */
    public function getQuote(): Quote
    {
        return $this->quote;
    }

    /**
     * @return ItemQuoteResponseInterface
     */
    public function getSimplerQuote(): ItemQuoteResponseInterface
    {
        return $this->simplerQuote;
    }
}
