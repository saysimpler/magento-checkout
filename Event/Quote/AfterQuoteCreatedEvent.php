<?php

namespace Simpler\Checkout\Event\Quote;

use Magento\Quote\Model\Quote;
use Simpler\Checkout\Event\Event;

class AfterQuoteCreatedEvent extends Event
{
    protected $name = 'simpler_checkout_quote_after_created';

    /**
     * @var Quote
     */
    private $quote;

    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * @return Quote
     */
    public function getQuote(): Quote
    {
        return $this->quote;
    }
}
