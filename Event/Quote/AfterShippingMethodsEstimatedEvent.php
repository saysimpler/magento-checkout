<?php

namespace Simpler\Checkout\Event\Quote;

use Magento\Quote\Api\Data\ShippingMethodInterface;
use Magento\Quote\Model\Quote;
use Simpler\Checkout\Event\Event;

class AfterShippingMethodsEstimatedEvent extends Event
{
    protected $name = 'simpler_checkout_quote_after_shipping_methods_estimated';

    /**
     * @var Quote
     */
    private $quote;
    /**
     * @var ShippingMethodInterface[]
     */
    private $shippingMethods;

    public function __construct(Quote $quote, array $shippingMethods = [])
    {
        $this->quote           = $quote;
        $this->shippingMethods = $shippingMethods;
    }

    /**
     * @return Quote
     */
    public function getQuote(): Quote
    {
        return $this->quote;
    }

    /**
     * @return ShippingMethodInterface[]
     */
    public function getShippingMethods(): array
    {
        return $this->shippingMethods;
    }
}
