<?php

namespace Simpler\Checkout\Event\Quote;

use Magento\Quote\Model\Quote;
use Simpler\Checkout\Api\Data\ItemInterface;
use Simpler\Checkout\Event\Event;

class AfterProductsAddedEvent extends Event
{
    protected $name = 'simpler_checkout_quote_after_products_added';

    /**
     * @var Quote
     */
    private $quote;
    /**
     * @var ItemInterface[]
     */
    private $items;

    public function __construct(Quote $quote, array $items = [])
    {
        $this->quote = $quote;
        $this->items = $items;
    }

    /**
     * @return ItemInterface[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return Quote
     */
    public function getQuote(): Quote
    {
        return $this->quote;
    }
}
