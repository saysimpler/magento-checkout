<?php

namespace Simpler\Checkout\Event\Quote;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Model\Quote;
use Simpler\Checkout\Event\Event;

class BeforeCustomerAssignedEvent extends Event
{
    protected $name = 'simpler_checkout_quote_before_customer_assigned';

    /**
     * @var Quote
     */
    private $quote;
    /**
     * @var CustomerInterface|null
     */
    private $customer;
    /**
     * @var AddressInterface
     */
    private $shippingAddress;
    /**
     * @var AddressInterface
     */
    private $billingAddress;

    public function __construct(
        Quote $quote,
        ?CustomerInterface $customer,
        AddressInterface $shippingAddress,
        AddressInterface $billingAddress
    ) {
        $this->quote           = $quote;
        $this->customer        = $customer;
        $this->shippingAddress = $shippingAddress;
        $this->billingAddress  = $billingAddress;
    }

    /**
     * @return Quote
     */
    public function getQuote(): Quote
    {
        return $this->quote;
    }

    /**
     * @return CustomerInterface|null
     */
    public function getCustomer(): ?CustomerInterface
    {
        return $this->customer;
    }

    /**
     * @return AddressInterface
     */
    public function getShippingAddress(): AddressInterface
    {
        return $this->shippingAddress;
    }

    /**
     * @return AddressInterface
     */
    public function getBillingAddress(): AddressInterface
    {
        return $this->billingAddress;
    }
}
