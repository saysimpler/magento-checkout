<?php

namespace Simpler\Checkout\Event\Products;

use Magento\Catalog\Api\Data\ProductInterface;
use Simpler\Checkout\Api\Data\ConfigurableOptionInterface;
use Simpler\Checkout\Event\Event;

class AfterProductConfigurationsCollectedEvent extends Event
{
    protected $name = 'simpler_checkout_products_after_configurations_collected';

    /**
     * @var ConfigurableOptionInterface $option
     */
    private $option;
    /**
     * @var ProductInterface $product
     */
    private $product;
    /**
     * @var array $configurableOptions
     */
    private $configurableOptions;

    public function __construct(ConfigurableOptionInterface $option, ProductInterface $product, array $configurableOptions)
    {
        $this->option = $option;
        $this->product = $product;
        $this->configurableOptions = $configurableOptions;
    }

    /**
     * @param ConfigurableOptionInterface $option
     */
    public function setOption(ConfigurableOptionInterface $option)
    {
        $this->option = $option;
    }

    /**
     * @return ConfigurableOptionInterface
     */
    public function getOption(): ConfigurableOptionInterface
    {
        return $this->option;
    }

    /**
     * @return ProductInterface
     */
    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    /**
     * @return array
     */
    public function getConfigurableOptions(): array
    {
        return $this->configurableOptions;
    }
}
