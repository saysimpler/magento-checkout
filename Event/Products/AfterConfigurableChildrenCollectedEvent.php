<?php

namespace Simpler\Checkout\Event\Products;

use Magento\Catalog\Api\Data\ProductInterface;
use Simpler\Checkout\Event\Event;

class AfterConfigurableChildrenCollectedEvent extends Event
{
    protected $name = 'simpler_checkout_products_after_configurable_children_collected';

    /**
     * @var ProductInterface $product
     */
    private $product;

    /**
     * @var array $childrenIds
     */
    private $childrenIds;

    public function __construct(ProductInterface $product, array $childrenIds)
    {
        $this->product = $product;
        $this->childrenIds = $childrenIds;
    }

    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    public function getChildrenIds(): array
    {
        return $this->childrenIds;
    }

    public function setChildrenIds($ids)
    {
        $this->childrenIds = $ids;
    }
}
