<?php

namespace Simpler\Checkout\Event\Products;

use Simpler\Checkout\Api\Data\ProductsResponseInterface;
use Simpler\Checkout\Event\Event;

class AfterProductsAddedEvent extends Event
{
    protected $name = 'simpler_checkout_products_after_products_added';

    /**
     * @var ProductsResponseInterface
     */
    private $response;

    public function __construct(ProductsResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * @return ProductsResponseInterface
     */
    public function getResponse(): ProductsResponseInterface
    {
        return $this->response;
    }

    /**
     * @param  ProductsResponseInterface  $response
     */
    public function setResponse(ProductsResponseInterface $response): void
    {
        $this->response = $response;
    }
}
