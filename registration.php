<?php

use Magento\Framework\Component\ComponentRegistrar;

const SIMPLER_CHECKOUT_MODULE_NAME = 'Simpler_Checkout';

ComponentRegistrar::register(ComponentRegistrar::MODULE, SIMPLER_CHECKOUT_MODULE_NAME, __DIR__);
