<?php

namespace Simpler\Checkout\Controller;

use Simpler\Checkout\Api\Data\QuoteRequestInterface;
use Simpler\Checkout\Api\Data\QuoteResponseInterfaceFactory;
use Simpler\Checkout\Exception\InvalidProductException;
use Simpler\Checkout\Exception\MissingQuotationException;
use Simpler\Checkout\Exception\OutOfStockProductException;
use Simpler\Checkout\Exception\UnavailableProductException;
use Simpler\Checkout\Exception\UnshippableLocationException;
use Simpler\Checkout\Model\Api\QuoteResponse;
use Simpler\Checkout\Model\Processors\Quote;

class QuotationController
{
    /**
     * @var Quote
     */
    private $quoteProcessor;
    /**
     * @var QuoteResponse
     */
    private $quotationResponse;

    /**
     * @param  Quote                          $quoteProcessor
     * @param  QuoteResponseInterfaceFactory  $quoteResponseFactory
     */
    public function __construct(
        Quote                         $quoteProcessor,
        QuoteResponseInterfaceFactory $quoteResponseFactory
    ) {
        $this->quoteProcessor = $quoteProcessor;
        $this->quotationResponse = $quoteResponseFactory->create();
    }

    /**
     * @throws UnavailableProductException
     * @throws UnshippableLocationException
     * @throws MissingQuotationException
     * @throws OutOfStockProductException
     * @throws InvalidProductException
     */
    public function handle(QuoteRequestInterface $request)
    {
        $quotations = $this->quoteProcessor->quote($request);
        $this->quotationResponse->setRequestId($request->getRequestId())->setQuotations($quotations);

        return $this->quotationResponse;
    }
}
