<?php

namespace Simpler\Checkout\Controller\Success;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\{Action, Context};
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\ResultFactory;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Spi\OrderResourceInterface;

class Clear extends Action
{

    private $session;
    private $quoteRepository;
    private $orderResource;
    private $orderFactory;
    private $request;

    public function __construct(
        Context $context,
        Http $request,
        Session $session,
        OrderResourceInterface $orderResource,
        OrderFactory $orderFactory,
        QuoteRepository $quoteRepository
    ) {
        $this->request = $request;
        $this->session = $session;
        $this->orderResource = $orderResource;
        $this->orderFactory = $orderFactory;
        $this->quoteRepository = $quoteRepository;
        return parent::__construct($context);
    }

    public function execute()
    {
        $quote = $this->session->getQuote();

        $this->quoteRepository->delete($quote);
        $this->session->clearQuote();
        // TODO : this doesn't seem to work (especially for the minicart)
        // could be because we're ending up in a redirect
        // to be re-evaluated when we open up the configurable thank you page task.
        $this->session->clearStorage();
        $this->session->clearHelperData();

        $orderId = $this->request->getParam('order_id');
        $order = $this->orderFactory->create();
        $this->orderResource->load($order, $orderId, OrderInterface::INCREMENT_ID);
        if ($order) {
            $this->session->setLastQuoteId($order->getQuoteId())
                ->setLastSuccessQuoteId($order->getQuoteId())
                ->setLastOrderId($order->getId())
                ->setLastRealOrderId($order->getIncrementId())
                ->setLastOrderStatus($order->getStatus());
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('checkout/onepage/success', ['_secure' => true]);
        return $resultRedirect;
    }
}
