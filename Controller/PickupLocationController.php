<?php

namespace Simpler\Checkout\Controller;

use Magento\Framework\Event\ManagerInterface;
use Simpler\Checkout\Api\Data\PickupLocationsInterfaceFactory;

class PickupLocationController
{
    /**
     * @var PickupLocationsInterfaceFactory
     */
    private $locationFactory;
    /**
     * @var ManagerInterface
     */
    private $eventManager;

    public function __construct(
        ManagerInterface $eventManager,
        PickupLocationsInterfaceFactory $locationsFactory
    ) {
        $this->locationFactory = $locationsFactory;
        $this->eventManager    = $eventManager;
    }

    public function index()
    {
        $locations = $this->locationFactory->create();
        $this->eventManager->dispatch('simpler_checkout_pickup_locations_requested', [
            'locations' => $locations
        ]);
        return $locations->getPickupLocations();
    }
}
