<?php

namespace Simpler\Checkout\Controller\Config;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Simpler\Checkout\Helper\ButtonHelper;

class Checkout implements ActionInterface
{

    private $session;
    private $jsonFactory;
    private $buttonHelper;

    public function __construct(
        Session $session,
        JsonFactory $jsonFactory,
        ButtonHelper $buttonHelper
    ) {
        $this->session = $session;
        $this->jsonFactory = $jsonFactory;
        $this->buttonHelper = $buttonHelper;
    }

    public function execute()
    {
        $result = $this->jsonFactory->create();
        $cart = $this->buttonHelper->buildCart(
            $this->session->getQuote()->getAllVisibleItems()
        );
        $result->setData([
            'shouldRender' => $this->buttonHelper->shouldRender(),
            'enabledPositions' => $this->buttonHelper->getEnabledPositions(),
            'attrs' => [
                'appId' => $this->buttonHelper->getAppId(),
                'locale' => $this->buttonHelper->getLocale(),
                'payload' => $this->buttonHelper->getPayload([
                    'items' => $cart,
                    'coupon' => $this->session->getQuote()->getCouponCode()
                ]),
                'withAcceptedCards' => true,
            ]
        ]);
        return $result;
    }
}
