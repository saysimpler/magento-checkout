<?php

namespace Simpler\Checkout\Controller;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InputMismatchException;
use Simpler\Checkout\Api\Data\SubmitRequestInterface;
use Simpler\Checkout\Api\Data\SubmitResponseInterface;
use Simpler\Checkout\Api\Data\SubmitResponseInterfaceFactory;
use Simpler\Checkout\Exception\InvalidProductException;
use Simpler\Checkout\Exception\OutOfStockProductException;
use Simpler\Checkout\Exception\UnavailableProductException;
use Simpler\Checkout\Model\Processors\Submit;

class OrderCreateController
{
    /**
     * @var Submit
     */
    private $submitProcessor;
    /**
     * @var SubmitResponseInterface
     */
    private $submitResponse;

    /**
     * @param  Submit                          $submitProcessor
     * @param  SubmitResponseInterfaceFactory  $submitResponseInterfaceFactory
     */
    public function __construct(
        Submit                         $submitProcessor,
        SubmitResponseInterfaceFactory $submitResponseInterfaceFactory
    ) {
        $this->submitProcessor = $submitProcessor;
        $this->submitResponse = $submitResponseInterfaceFactory->create();
    }


    /**
     * @throws CouldNotSaveException
     * @throws InputMismatchException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws InputException
     * @throws InvalidProductException
     * @throws UnavailableProductException
     * @throws OutOfStockProductException
     */
    public function handle(SubmitRequestInterface $request): SubmitResponseInterface
    {
        $orderId = $this->submitProcessor->submit($request);
        $this->submitResponse->setExternalId($orderId)->setRequestId($request->getRequestId());

        return $this->submitResponse;
    }
}
