<?php

namespace Simpler\Checkout\Notification;

use Magento\Framework\Module\Manager;
use Magento\Framework\Notification\MessageInterface;
use Simpler\Checkout\Helper\SimplerConfig;

class EmptyAppSecret implements MessageInterface
{
    /**
     * @var SimplerConfig
     */
    private $simplerConfig;
    /**
     * @var Manager
     */
    private $moduleManager;

    public function __construct(SimplerConfig $simplerConfig, Manager $moduleManager)
    {
        $this->simplerConfig = $simplerConfig;
        $this->moduleManager = $moduleManager;
    }

    private const MESSAGE_IDENTITY = 'simpler_checkout_empty_app_secret_system_notification';

    public function getIdentity(): string
    {
        return self::MESSAGE_IDENTITY;
    }

    public function isDisplayed(): bool
    {
        return $this->moduleManager->isEnabled(SIMPLER_CHECKOUT_MODULE_NAME) && $this->simplerConfig->getAppSecret() === null;
    }

    public function getText()
    {
        $env = $this->simplerConfig->getEnvName();

        return __("Simpler Checkout ($env): Missing required 'App Secret'.");
    }

    public function getSeverity(): int
    {
        return self::SEVERITY_MINOR;
    }
}
