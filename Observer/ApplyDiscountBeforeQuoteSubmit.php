<?php

namespace Simpler\Checkout\Observer;

use Magento\Framework\Event\{Observer, ObserverInterface};
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Quote\Model\Quote;
use Simpler\Checkout\Helper\Money;

class ApplyDiscountBeforeQuoteSubmit implements ObserverInterface
{

    const DESCRIPTION_TEXT = "Simpler Discount";

    /** @var OrderRepositoryInterface */
    private $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function execute(Observer $observer)
    {
        /** @var Quote $quote */
        $quote = $observer->getData('quote');

        /** @var float $amount */
        $amountCents = $quote->getData('simpler_discount_cents');
        if (!$amountCents) {
            return;
        }

        $amount = Money::fromCents($amountCents);

        /** @var OrderInterface $order */
        $order = $observer->getData('order');

        $discountDescription = $order->getDiscountDescription() ? sprintf(
            '%s, %s',
            $order->getDiscountDescription(),
            static::DESCRIPTION_TEXT
        ) : static::DESCRIPTION_TEXT;

        $order->setDiscountDescription($discountDescription);

        $order->setBaseDiscountAmount($order->getBaseDiscountAmount() - $amount);
        $order->setDiscountAmount($order->getDiscountAmount() - $amount);

        $order->setBaseGrandTotal($order->getBaseGrandTotal() - $amount);
        $order->setGrandTotal($order->getGrandTotal() - $amount);

        $order->setBaseTotalPaid($order->getBaseGrandTotal());
        $order->setTotalPaid($order->getGrandTotal());

        $order->setBaseTotalInvoiced($order->getBaseGrandTotal());
        $order->setTotalInvoiced($order->getGrandTotal());

        $this->orderRepository->save($order);
    }
}
