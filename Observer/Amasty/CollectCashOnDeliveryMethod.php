<?php

namespace Simpler\Checkout\Observer\Amasty;

use Exception;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\{Observer, ObserverInterface};
use Magento\Framework\Module\Manager as ModuleManager;
use Magento\OfflinePayments\Model\Cashondelivery;
use Simpler\Checkout\Helper\Money;

class CollectCashOnDeliveryMethod implements ObserverInterface
{
    /**
     * @var ModuleManager
     */
    private $moduleManager;


    public function __construct(ModuleManager $manager)
    {
        $this->moduleManager = $manager;
    }

    public function execute(Observer $observer)
    {
        if (!$this->moduleManager->isEnabled('Amasty_CashOnDelivery')) {
            return;
        }

        $event = $observer->getData('simpler_checkout_event');
        /**
         * @var \Magento\Quote\Model\Quote $quote
         */
        $quote = $event->getQuote();
        $quote->setPaymentMethod(Cashondelivery::PAYMENT_METHOD_CASHONDELIVERY_CODE)
            ->setTotalsCollectedFlag(false)
            ->collectTotals();

        try {
            $pfRepo = ObjectManager::getInstance()->create('\Amasty\CashOnDelivery\Model\Repository\PaymentFeeRepository');

            /**
             * @var Amasty\CashOnDelivery\Model\PaymentFee $fee
             */
            $fee = $pfRepo->getByQuoteId($quote->getId());

            /**
             * @var \Simpler\Checkout\Api\Data\PaymentMethodInterface[] $paymentMethods
             */
            $paymentMethods = $event->getPaymentMethods();
            foreach ($paymentMethods as $pm) {
                if ($pm->getId() === Cashondelivery::PAYMENT_METHOD_CASHONDELIVERY_CODE) {
                    $pm->setNetCents(Money::toCents($fee->getBaseAmount()))
                        ->setTaxCents(Money::toCents($fee->getTaxAmount()))
                        ->setTotalCents(Money::toCents($fee->getAmount()));
                }
            }
        } catch (Exception $e) {
            // no fee configured
        }
    }
}
