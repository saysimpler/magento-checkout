<?php

namespace Simpler\Checkout\Observer\Amasty;

use Magento\Framework\Event\{Observer, ObserverInterface};
use Magento\Framework\Module\Manager;

class ClearDiscountCache implements ObserverInterface
{
    /**
     * @var Manager
     */
    private $moduleManager;

    public function __construct(Manager $manager)
    {
        $this->moduleManager = $manager;
    }

    public function execute(Observer $observer)
    {
        if (!$this->moduleManager->isEnabled('Amasty_Rules')) {
            return;
        }

        if (class_exists('\Amasty\Rules\Model\Rule\Action\Discount\Groupn')) {
            \Amasty\Rules\Model\Rule\Action\Discount\Groupn::$cachedDiscount = [];
        }
    }
}
