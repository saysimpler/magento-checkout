<?php

namespace Simpler\Checkout\Observer\Elegento;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Module\Manager;
use Magento\Quote\Api\CartRepositoryInterface;

class BoxNowQuoteLockerId implements ObserverInterface
{
    /**
     * @var Manager
     */
    private $moduleManager;
    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    public function __construct(Manager $manager, CartRepositoryInterface $quoteRepository)
    {
        $this->moduleManager = $manager;
        $this->quoteRepository = $quoteRepository;
    }

    public function execute(Observer $observer)
    {
        if (!$this->moduleManager->isEnabled("Elegento_BoxNow")) {
            return;
        }

        $quote = $observer->getData('quote');
        $request = $observer->getData('request');

        if ($bn = $request->getBoxNow()) {
            $boxnowData = json_encode([
                'boxnowLockerId'           => $bn->getLockerId(),
                'boxnowLockerAddressLine1' => $bn->getAddress(),
                'boxnowLockerPostalCode'   => $bn->getPostcode(),
            ]);
            $quote->setBoxnowId($boxnowData);
            $this->quoteRepository->save($quote);
        }
    }
}
