<?php

namespace Simpler\Checkout\Observer\Elegento;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Module\Manager;

class BoxNowSetLockerId implements ObserverInterface
{
    /**
     * @var Manager
     */
    private $moduleManager;

    public function __construct(Manager $manager)
    {
        $this->moduleManager = $manager;
    }

    public function execute(Observer $observer)
    {
        if (!$this->moduleManager->isEnabled("Elegento_BoxNow")) {
            return;
        }

        $order = $observer->getData('order');
        $request = $observer->getData('request');

        if ($bn = $request->getBoxNow()) {
            $boxnowData = json_encode([
                'boxnowLockerId'           => $bn->getLockerId(),
                'boxnowLockerAddressLine1' => $bn->getAddress(),
                'boxnowLockerPostalCode'   => $bn->getPostcode(),
            ]);

            // one version
            try {
                $order->setBoxnowId($boxnowData);
                $description = $bn->getLockerId() . ':BoxNow - ' . $bn->getAddress() . ', TK ' . $bn->getPostcode();
                $order->setShippingDescription($description);
            } catch (\Exception $e) {
                // do nothing
            }

            // another version
            try {
                $order->getShippingAddress()->setData('boxnow_id', $boxnowData);
            } catch (\Exception $e) {
                // do nothing
            }
        }
    }
}
