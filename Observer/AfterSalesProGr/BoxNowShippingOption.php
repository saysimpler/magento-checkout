<?php

namespace Simpler\Checkout\Observer\AfterSalesProGr;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Module\Manager;

class BoxNowShippingOption implements ObserverInterface
{

    /**
     * @var Manager
     */
    private $moduleManager;

    public function __construct(Manager $manager)
    {
        $this->moduleManager = $manager;
    }

    public function execute(Observer $observer)
    {
        if (!$this->moduleManager->isEnabled("AfterSalesProGr_ShippingPoints")) {
            return;
        }

        if (!class_exists('\AfterSalesProGr\ShippingPoints\Model\Carrier\Custom')) {
            error_log('ShippingPoints module enabled, but custom carrier class does not exist');
            return;
        }

        /** @var ShippingMethod $method  */
        $method = $observer->getData('shipping_method');

        if ($method->getCarrierCode() == \AfterSalesProGr\ShippingPoints\Model\Carrier\Custom::METHOD_CODE) {
            $option = $observer->getData('shipping_option');
            $option->setType("BOX_NOW");
        }
    }
}
