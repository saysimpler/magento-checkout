<?php

namespace Simpler\Checkout\Observer\AfterSalesProGr;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Module\Manager;
use Magento\Sales\Api\OrderRepositoryInterface;

class BoxNowSetLockerId implements ObserverInterface
{

    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(Manager $manager, OrderRepositoryInterface $orderRepository)
    {
        $this->moduleManager = $manager;
        $this->orderRepository = $orderRepository;
    }

    public function execute(Observer $observer)
    {
        if (!$this->moduleManager->isEnabled("AfterSalesProGr_ShippingPoints")) {
            return;
        }

        $order = $observer->getData('order');
        $request = $observer->getData('request');

        if ($bn = $request->getBoxNow()) {
            $order->setAspSpPointCarrierSlug('boxnowgr')
                ->setAspSpPointId($bn->getLockerId());
        }
    }
}
