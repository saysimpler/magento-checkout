<?php

namespace Simpler\Checkout\Helper;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Module\Manager;
use Magento\InventoryCatalog\Model\GetStockIdForCurrentWebsite;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;

class StockHelper extends AbstractHelper
{
    /**
     * @var bool $isInventoryEnabled
     */
    private $isInventoryEnabled;

    /**
     * @var StockStateInterface|null $stockState
     */
    private $stockState;

    /**
     * @var GetProductSalableQtyInterface|null $getSalableQty
     */
    private $getSalableQty;

    /**
     * @var GetStockIdForCurrentWebsite|null $getStockIdForCurrentWebsite
     */
    private $getStockIdForCurrentWebsite;

    public function __construct(Manager $moduleManager)
    {
        $this->isInventoryEnabled = $moduleManager->isEnabled("Magento_Inventory");
        if ($this->isInventoryEnabled) {
            $this->getStockIdForCurrentWebsite = ObjectManager::getInstance()->create(GetStockIdForCurrentWebsite::class);
            $this->getSalableQty = ObjectManager::getInstance()->create(GetProductSalableQtyInterface::class);
        } else {
            $this->stockState = ObjectManager::getInstance()->create(StockStateInterface::class);
        }
    }

    public function getStockQuantity(ProductInterface $product)
    {
        if ($this->isInventoryEnabled) {
            $stockId = $this->getStockIdForCurrentWebsite->execute();
            $stock = $this->getSalableQty->execute($product->getData('sku'), $stockId);
            return $stock;
        } else {
            return $this->stockState->getStockQty($product->getId());
        }
    }
}
