<?php

namespace Simpler\Checkout\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * @package Simpler\Checkout\Helper
 */
class SimplerConfig extends AbstractHelper implements ScopeInterface
{
    public const ENABLED_FLAG          = 'simpler_checkout/settings/enable';

    public const MERCHANT_ID_LIVE      = 'simpler_checkout_credentials/live/merchant_id';
    public const APP_SECRET_LIVE       = 'simpler_checkout_credentials/live/app_secret';

    public const REFUNDS_URL_LIVE      = 'simpler_checkout/api/live/refunds';

    public const SHOW_IN_PRODUCT_VIEW  = 'simpler_checkout/settings/show_in_product_view';
    public const SHOW_IN_CART_VIEW     = 'simpler_checkout/settings/show_in_cart_view';
    public const SHOW_IN_CHECKOUT_VIEW = 'simpler_checkout/settings/show_in_checkout_view';
    public const SHOW_IN_MINICART      = 'simpler_checkout/settings/show_in_minicart';
    public const SKIP_SALEABILITY_CHECK = 'simpler_checkout/settings/skip_saleability_check';
    public const APPLICABLE_ORDER_STATUS = 'simpler_checkout/advanced/applicable_order_status';
    public const SDK_URL_LIVE          = 'simpler_checkout/sdk/live';

    public const MODULE_NAME      = 'Simpler_Checkout';
    public const ENV_LIVE_NAME    = 'Live';

    /**
     * @var ModuleListInterface
     */
    private $moduleList;
    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * Data constructor.
     *
     * @param  Context              $context
     * @param  EncryptorInterface   $encryptor
     * @param  ModuleListInterface  $moduleList
     */
    public function __construct(
        Context             $context,
        EncryptorInterface  $encryptor,
        ModuleListInterface $moduleList
    ) {
        parent::__construct($context);
        $this->moduleList = $moduleList;
        $this->encryptor = $encryptor;
    }

    /**
     * Retrieve module current version
     *
     * @return string
     */
    public function getModuleVersion(): string
    {
        return $this->moduleList->getOne(self::MODULE_NAME)['setup_version'];
    }

    /**
     * Retrieve config value
     *
     * @param $config
     * @return ?string
     */
    public function getConfig($config): ?string
    {
        return $this->scopeConfig->getValue($config, self::SCOPE_STORE);
    }

    /**
     * Get merchant id
     *
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->getConfig(self::MERCHANT_ID_LIVE);
    }

    /**
     * Get App Secret
     *
     * @return string|null
     */
    public function getAppSecret(): ?string
    {
        $secret = $this->getConfig(self::APP_SECRET_LIVE);

        return $this->encryptor->decrypt($secret);
    }

    public function getRefundsURL(): string
    {
        return $this->getConfig(self::REFUNDS_URL_LIVE);
    }

    public function isEnabled(): bool
    {
        return $this->getConfig(self::ENABLED_FLAG);
    }

    public function getEnvName(): string
    {
        return self::ENV_LIVE_NAME;
    }

    public function getSDKURL(): string
    {
        return $this->getConfig(self::SDK_URL_LIVE);
    }
}
