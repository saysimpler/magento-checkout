<?php

namespace Simpler\Checkout\Helper;

class Money
{
    public static function toCents($amount): int
    {
        $rounded = round($amount, 2);
        return intval(strval($rounded * 100));
    }

    public static function fromCents($amount): float
    {
        return round($amount / 100.0, 2);
    }
}
