<?php

namespace Simpler\Checkout\Helper;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Event\Manager;
use Magento\Framework\Locale\Resolver as LocaleResolver;
use Magento\Store\Model\StoreManagerInterface;

class ButtonHelper extends AbstractHelper
{
    /**
     * @var Manager $eventManager
     */
    private $eventManager;
    /**
     * @var LocaleResolver $localeResolver
     */
    private $localeResolver;
    /**
     * @var StoreManagerInterface $storeManager
     */
    private $storeManager;
    /**
     * @var SimplerConfig $simplerConfig
     */
    private $simplerConfig;
    /**
     * @var StockHelper $stockHelper;
     */
    private $stockHelper;

    public function __construct(
        Manager $eventManager,
        LocaleResolver $localeResolver,
        StoreManagerInterface $storeManager,
        SimplerConfig $simplerConfig,
        StockHelper $stockHelper
    ) {
        $this->eventManager = $eventManager;
        $this->localeResolver = $localeResolver;
        $this->storeManager = $storeManager;
        $this->simplerConfig = $simplerConfig;
        $this->stockHelper = $stockHelper;
    }

    public function shouldRender(): bool
    {
        return $this->simplerConfig->isEnabled() && $this->getAppId() != NULL;
    }

    /**
     * @param Magento\Quote\Model\Quote\Item[] $items
     */
    public function buildCart($items)
    {
        return array_map(function ($item) {
            if ($item->getChildren() != NULL && count($item->getChildren()) > 0) {
                return $this->buildCartItem($item->getChildren()[0]->getProduct(), $item->getQty(), $item->getOptions());
            } else {
                return $this->buildCartItem($item->getProduct(), $item->getQty(), $item->getOptions());
            }
        }, $items);
    }

    /**
     * @param ProductInterface $product
     * @param int $quantity
     */
    public function buildCartItem($product, $quantity = 0, $options = [])
    {
        $stock = $this->stockHelper->getStockQuantity($product);

        $attrs = [];
        if (method_exists($product, 'getTypeInstance')) {
            $orderOptions = $product->getTypeInstance(true)->getOrderOptions($product);
            foreach ($orderOptions['options'] ?? [] as $opt) {
                $attrs[$opt['option_id']] = $opt['option_value'];
            }
        }

        $this->eventManager->dispatch('simpler_checkout_prepare_cart_item_options', [
            'product' => $product,
            'quote_item_options' => $options,
            'attributes' => $attrs,
        ]);

        return [
            'product_sku' => $product->getData('sku'),
            'product_type' => $product->getTypeId(),
            'quantity' => $quantity,
            'attributes' => $attrs,
            'in_stock' => $stock > 0,
            'stock_quantity' => $stock
        ];
    }

    /**
     * @param array $cart
     */
    public function getPayload($cart)
    {
        return base64_encode(json_encode([
            'version' => $this->simplerConfig->getModuleVersion(),
            'cart' => $cart['items'],
            'coupon' => $cart['coupon'] ?? NULL,
            'currency' => $this->storeManager->getStore()->getCurrentCurrencyCode(),
            'storeId' => $this->storeManager->getStore()->getStoreId(),
            'websiteId' => $this->storeManager->getWebsite()->getId()
        ]));
    }

    /**
     * Get App ID
     *
     * @return string|null
     */
    public function getAppId(): ?string
    {
        return $this->simplerConfig->getMerchantId();
    }

    /**
     * Get Current Locale
     *
     * @return string
     */
    public function getLocale(): string
    {
        return $this->localeResolver->getLocale();
    }

    public function getEnabledPositions()
    {
        return [
            'simpler.checkout.page' => (bool)($this->simplerConfig->getConfig(SimplerConfig::SHOW_IN_CHECKOUT_VIEW)),
            'simpler.minicart' => (bool)($this->simplerConfig->getConfig(SimplerConfig::SHOW_IN_MINICART))
        ];
    }
}
