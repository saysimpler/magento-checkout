<?php

namespace Simpler\Checkout\Block\Frontend;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Simpler\Checkout\Helper\SimplerConfig;

class Head extends Template
{
    /**
     * @var SimplerConfig
     */
    private $simplerConfig;

    public function __construct(Context $context, SimplerConfig $simplerConfig)
    {
        parent::__construct($context);
        $this->simplerConfig = $simplerConfig;
    }

    public function render(): string
    {
        // It requires running bin/magento cache:clean for this to take effect
        $html = sprintf('<script type="text/javascript">window.simplerCheckoutAppId = "%s";</script>', $this->simplerConfig->getMerchantId());
        $html .= sprintf('<script type="text/javascript" src="%s"></script>', $this->simplerConfig->getSDKURL());
        return $html;
    }
}
