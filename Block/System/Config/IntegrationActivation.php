<?php

namespace Simpler\Checkout\Block\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;

class IntegrationActivation extends Field
{
    protected $_template = 'Simpler_Checkout::system/config/integration_activation.phtml';

    private $url;

    public function __construct(Context $context, UrlInterface $url)
    {
        parent::__construct($context);
        $this->url = $url;
    }

    public function render(AbstractElement $element)
    {
        return parent::render($element);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getCustomUrl()
    {
        return $this->url->getUrl('adminhtml/integration/index');
    }
}