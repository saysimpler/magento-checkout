<?php

namespace Simpler\Checkout\Exception;

use Throwable;

class InapplicableCouponException extends QuotationException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, 'INAPPLICABLE_COUPON', $code, $previous);
    }
}
