<?php

namespace Simpler\Checkout\Exception;

use Throwable;
use Magento\Framework\Webapi\Exception;

class QuotationException extends Exception
{
    public function __construct($message = "", string $errorCode = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            __('Failed to quote cart'),
            $code,
            Exception::HTTP_BAD_REQUEST,
            ['code' => $errorCode, 'error' => $message], null, [], $previous ? $previous->getTraceAsString() : null
        );
    }
}
