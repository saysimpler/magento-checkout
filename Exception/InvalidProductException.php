<?php

namespace Simpler\Checkout\Exception;

use Throwable;

class InvalidProductException extends QuotationException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, 'INVALID_PRODUCT', $code, $previous);
    }
}
