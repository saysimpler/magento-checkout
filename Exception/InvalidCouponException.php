<?php

namespace Simpler\Checkout\Exception;

use Throwable;

class InvalidCouponException extends QuotationException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, 'INVALID_COUPON', $code, $previous);
    }
}
