<?php

namespace Simpler\Checkout\Exception;

use Throwable;

class UnavailableProductException extends QuotationException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, 'UNAVAILABLE_PRODUCT', $code, $previous);
    }
}
