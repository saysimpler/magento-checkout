<?php

namespace Simpler\Checkout\Exception;

use Throwable;

class UnshippableLocationException extends QuotationException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, 'UNSHIPPABLE_LOCATION', $code, $previous);
    }
}
