<?php

namespace Simpler\Checkout\Exception;

use Throwable;

class MissingQuotationException extends QuotationException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, 'MISSING_QUOTATION', $code, $previous);
    }
}
