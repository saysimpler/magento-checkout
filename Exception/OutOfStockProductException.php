<?php

namespace Simpler\Checkout\Exception;

use Throwable;

class OutOfStockProductException extends QuotationException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, 'OUT_OF_STOCK_PRODUCT', $code, $previous);
    }
}
