<?php

namespace Simpler\Checkout\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

class IsSimpler implements OptionSourceInterface
{

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            [
                "value" => 1,
                "label" => __("Yes")
            ],
            [
                "value" => 0,
                "label" => __("No")
            ],
        ];
    }
}
